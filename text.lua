local text = {
	fontpath = "fnt;font;"
}

local Fonts = class()
text.newFonts = Fonts

function Fonts:_init()
	self.fonts = {}
end

local function getFontName(file, size, bold, italic, fonttype)
	if fonttype ~= "ttf" and size then
		file = file..'_'..size
	end
	if bold then
		file = file..'B'
	end
	if italic then
		file = file..'I'
	end
	if fonttype then
		file = file..'.'..fonttype
	end
	return file
end

function Fonts:load(fontfamily, size, bold, italic, fonttype)
	local fontname = getFontName(fontfamily, size, bold, italic, fonttype)
	local font = self.fonts[fontname]
	if font then
		return
	end
	for fontdir in text.fontpath:gmatch("(.-);") do
		if love.filesystem.getInfo(fontdir) then
			local fontpath = fontdir..'/'..fontname
			if love.filesystem.getInfo(fontpath) then
				if fonttype == "ttf" then
					font = love.graphics.newFont(fontpath, size)
				else
					font = love.graphics.newFont(fontpath)
				end
			end

			if font then
				font:setFilter("nearest", "nearest")
				self.fonts[fontname] = font
				return
			end
		end
	end
end

function Fonts:use(fontfamily, size, bold, italic, fonttype)
	local fontname = getFontName(fontfamily, size, bold, italic, fonttype)
	local font = self.fonts[fontname]
	if font then
		love.graphics.setFont(font)
	end
	return font
end

return text
