﻿---User preferences
--@field drawstats Draw performance stats (boolean)
--@field drawbodies Draw physics bodies (boolean)
--@field rotation Orientation of the screen in degrees (int)
--@field fullscreen (boolean)
--@field vsync (boolean)
--@field canvasresolution "LOW" == base resolution, "HIGH" == highest resolution allowed by the window size
--@field musicvolume (float)
--@field soundvolume (float)
--@table Prefs

local rawget = rawget
local pl_tablex = require("pl.tablex")

local prefsmodule = {
	prefs = {},
	isdisplaymodepref = {
		fullscreen = true,
		vsync = true,
		usedpiscale = true,
		rotation = true
	},
	defaultprefs = {
		_version = 1,
		drawbodies = false,
		drawstats = false,
		exclusive = false,
		exhibit = false,
		rotation = 0,
		fullscreen = true,
		vsync = true,
		usedpiscale = false,
		canvasresolution = "HIGH",
		canvasscaleint = true,
		canvasscalesoft = false,
		musicvolume = 1,
		soundvolume = 1,
		joy_namingscheme = "XBOX"
	}
}

function prefsmodule.copy()
	return pl_tablex.copy(prefsmodule.prefs)
end

function prefsmodule.reset()
	love.filesystem.remove("prefs.cfg")
	prefsmodule.init()
end

function prefsmodule.init()
	local prefs = {}
	prefsmodule.prefs = prefs

	return prefsmodule.load()

	--- Lock out against creating new prefs
	--setmetatable(prefs, {
	--	__index = function (prefs, key)
	--		print("prefs: Unknown pref "..key)
	--	end,
        --
	--	__newindex = function (prefs, key, value)
	--		print("prefs: Unknown pref "..key)
	--	end
	--})
end

local function showMessageBox_diffversion()
	local responses_diffversion = {
		"Reset to defaults", "Continue"
	}

	local response = love.window.showMessageBox(love.window.getTitle(), [[
Game options have changed since you last played.
You can reset to defaults and set everything again to be safe.

Or you can continue with your existing settings.
But some settings may work differently or not apply anymore, until you set them again.
]], responses_diffversion, "warning")

	return responses_diffversion[response]
end

function prefsmodule.isSaved()
	return love.filesystem.getInfo("prefs.cfg")
end

function prefsmodule.load()
	local prefs = prefsmodule.prefs
	for k, v in pairs(prefsmodule.defaultprefs) do
		prefs[k] = v
	end
	if love.filesystem.getInfo("prefs.cfg") then
		local newprefs = {}
		for line in love.filesystem.lines("prefs.cfg") do
			local k, v = line:match("([%w_]+) ([^\n]+)")
			if prefsmodule.defaultprefs[k] ~= nil then
				if v == "true" then
					newprefs[k] = true
				elseif v == "false" then
					newprefs[k] = false
				elseif k and v then
					newprefs[k] = tonumber(v) or v
				end
			else
				print("Ignored unknown pref: "..k)
			end
		end

		local response
		if newprefs._version ~= prefsmodule._version then
			response = showMessageBox_diffversion()
		end

		if response == "Reset to defaults" then
			love.filesystem.remove("prefs.cfg")
		else
			newprefs._version = prefsmodule._version
			for k, v in pairs(newprefs) do
				prefs[k] = v
			end
		end
	end
end

function prefsmodule.save(prefs)
	prefs = prefs or prefsmodule.prefs
	local PrefLine = "%s %s\n"

	local prefslines = ""
	for k, v in pl_tablex.sort(prefs) do
		prefslines = prefslines .. PrefLine:format(k, v)
	end

	local ok, err = love.filesystem.write("prefs.cfg", prefslines)
	if not ok then
		print(err)
	end
end

function prefsmodule.set(pref, value)
	local prefs = prefsmodule.prefs
	if type(pref) == "table" then
		for k,v in pairs(pref) do
			prefsmodule.set(k, v)
		end
	else
		prefs[pref] = value
		prefsmodule.apply(pref)
	end
end

function prefsmodule.isRotatedVertical()
	local rotation = math.rad(prefsmodule.rotation)
	return math.abs(math.sin(rotation)) > math.sqrt(2)/2
end

function prefsmodule.isWindowVertical()
	local w, h, flags = love.window.getMode()
	return w < h
end

function prefsmodule.setApplyFunction(pref, func)
	rawset(prefsmodule, "apply_"..pref, func)
end

function prefsmodule.apply(pref)
	local apply = rawget(prefsmodule, "apply_"..pref)
	if apply then
		apply(prefsmodule.prefs[pref])
	end
end

function prefsmodule.applyDisplayMode()
	local w, h, flags = love.window.getMode()
	local modes = love.window.getFullscreenModes()
	local exclusive = prefsmodule.exclusive
	local fullscreen = prefsmodule.fullscreen
	local basew, baseh
	if prefsmodule.isRotatedVertical() then
		basew = prefsmodule.basewindowheight
		baseh = prefsmodule.basewindowwidth
	else
		basew = prefsmodule.basewindowwidth
		baseh = prefsmodule.basewindowheight
	end
	local bestmode
	local maxscale = 1

	if fullscreen and exclusive then
		for i = 1, #modes do
			local mode = modes[i]
			if not bestmode
			or bestmode.width > mode.width
			or bestmode.height > mode.height
			then
				if mode.height >= baseh and mode.width >= basew then
					bestmode = mode
				end
			end
		end
		if bestmode then
			maxscale = math.min(bestmode.width/basew, bestmode.height/baseh)
		end
	else
		local deskwidth, deskheight = love.window.getDesktopDimensions()
		maxscale = math.min(deskwidth/basew, deskheight/baseh)
	end
	maxscale = math.floor(maxscale)
	w = basew*maxscale
	h = baseh*maxscale

	flags.fullscreen = fullscreen
	flags.fullscreentype = exclusive and "exclusive" or "desktop"
	flags.usedpiscale = prefsmodule.usedpiscale
	flags.vsync = prefsmodule.vsync
	flags.resizable = prefsmodule.resizable
	flags.x = nil
	flags.y = nil
	love.window.setMode(w, h, flags)
	love.event.push("resize", w, h)
end

function prefsmodule.applyAll()
	prefsmodule.applyDisplayMode()
	local prefs = prefsmodule.prefs
	local isdisplaymodepref = prefsmodule.isdisplaymodepref
	for k, v in pairs(prefs) do
		if not isdisplaymodepref[k] then
			prefsmodule.apply(k)
		end
	end
end

function prefsmodule.apply_canvasresolution()
	local w, h, flags = love.window.getMode()
	love.event.push("resize", w, h)
end
prefsmodule.apply_canvasscaleint = prefsmodule.apply_canvasresolution
prefsmodule.apply_canvasscalesoft = prefsmodule.apply_canvasresolution

function prefsmodule.apply_musicvolume(volume)
	local levity = require "levity"
	if levity.bank then
		levity.bank:setMusicVolume(volume)
	end
end

local NamingSchemes = {
	XBOX = {
		left = "Left Stick",
		right = "Right Stick",
		leftx = "Left Stick ⟷",
		lefty = "Left Stick ↕",
		rightx = "Right Stick ⟷",
		righty = "Right Stick ↕",
		leftstick = "LS Click",
		rightstick = "RS Click",
		triggerleft = "LT",
		triggerright = "RT",
		dp = "D-pad",
		x = "X",
		y = "Y",
		a = "A",
		b = "B",
		back = "Back",
		start = "Start",
		guide = "Guide",
		leftshoulder = "LB",
		rightshoulder = "RB",
	},
	PS3 = {
		left = "Left Stick",
		right = "Right Stick",
		leftx = "Left Stick ⟷",
		lefty = "Left Stick ↕",
		rightx = "Right Stick ⟷",
		righty = "Right Stick ↕",
		leftstick = "LS Click",
		rightstick = "RS Click",
		triggerleft = "L2",
		triggerright = "R2",
		dp = "D-pad",
		x = "□",
		y = "△",
		a = "⨯",
		b = "○",
		back = "Select",
		start = "Start",
		guide = "PS",
		leftshoulder = "L1",
		rightshoulder = "R1",
	},
	PS4 = {
		left = "Left Stick",
		right = "Right Stick",
		leftx = "Left Stick ⟷",
		lefty = "Left Stick ↕",
		rightx = "Right Stick ⟷",
		righty = "Right Stick ↕",
		leftstick = "LS Click",
		rightstick = "RS Click",
		triggerleft = "L2",
		triggerright = "R2",
		dp = "D-pad",
		x = "□",
		y = "△",
		a = "⨯",
		b = "○",
		back = "Share",
		start = "Options",
		guide = "PS",
		leftshoulder = "L1",
		rightshoulder = "R1",
	},
	SWITCH = {
		left = "Left Stick",
		right = "Right Stick",
		leftx = "Left Stick ⟷",
		lefty = "Left Stick ↕",
		rightx = "Right Stick ⟷",
		righty = "Right Stick ↕",
		leftstick = "LS Click",
		rightstick = "RS Click",
		triggerleft = "ZL",
		triggerright = "ZR",
		dp = "+Control Pad",
		x = "Y",
		y = "X",
		a = "B",
		b = "A",
		back = "-",
		start = "+",
		guide = "Home",
		leftshoulder = "L",
		rightshoulder = "R",
	}
}

-- similar and synonyms
NamingSchemeSynonyms = {
	PS1 = "PS3",
	PS2 = "PS3",
	["PLAYSTATION%(R%)3"] = "PS3",
	["PLAYSTATION%(R%)4"] = "PS4",
	WII = "SWITCH",
	XINPUT = "XBOX",
	["X-BOX"] = "XBOX",
	["DUALSHOCK 4"] = "PS4"
}

function prefsmodule.guessJoystickNamingScheme(joystickname)
	joystickname = joystickname:upper()
	for scheme, names in pairs(NamingSchemes) do
		if joystickname:match(scheme) then
			return scheme
		end
	end
	for synonym, scheme in pairs(NamingSchemeSynonyms) do
		if joystickname:match(synonym) then
			return scheme
		end
	end
	return "XBOX"
end

local function getJoystickInputName(scheme, input)
	local names = NamingSchemes[scheme]
	if not names then
		return input
	end
	if input:find("dp") == 1 then
		input = "dp"
	end
	return names[input] or input
end

--- Search string for ${PREF} tokens and replace with pref values
function prefsmodule.string_gsub(str)
	local prefs = prefsmodule.prefs
	local scheme = prefs.joy_namingscheme
	local pref = str:match("${([%w_]+)}")
	while pref do
		local value = tostring(prefs[pref])
		if value then
			local prefix = pref:match("^%w+_")
			if prefix == "joy_" then
				value = getJoystickInputName(scheme, value)
			elseif prefix == "key_" then
				value = value:upper()
			end
			if value == "true" or value == "TRUE" then
				value = "ON"
			elseif value == "false" or value == "FALSE" then
				value = "OFF"
			end
		else
			value = pref
		end
		str = str:gsub("${[%w_]+}", value, 1)
		pref = str:match("${([%w_]+)}")
	end
	return str
end

--- Fall through to actual prefs table
setmetatable(prefsmodule, {
	__index = function(prefsmodule, key)
		return prefsmodule.prefs[key]
	end,

	__newindex = function(prefsmodule, key, value)
		prefsmodule.prefs[key] = value
	end
})

--prefsmodule.init()
return prefsmodule
