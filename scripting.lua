-- @module scripting

local type = type
local pairs = pairs
local rawset = rawset
local setmetatable = setmetatable
local table_remove = table.remove

local pl_tablex = require("pl.tablex")
local pl_tablex_clear = pl_tablex.clear
local pl_tablex_find = pl_tablex.find

local _eventscripttables
local _idscriptarrays
local _scriptlogs
--local _classes

local _broadcastdepth

local Machine = class()
function Machine:_init()
	_eventscripttables = {}
	_idscriptarrays = {}
	_scriptlogs = nil
	_broadcastdepth = 0
	--_classes = {}
end

--- Get a script class, loading it if not loaded already
-- @name of the class (not file name or path)
-- @return The script class
--function Machine:requireScript(name)
--	local scriptclass = _classes[name]
--	if not scriptclass then
--		scriptclass = love.filesystem.load(name..".lua")()
--		_classes[name] = scriptclass
--	end
--	return scriptclass
--end

--- Make a script start responding to a type of event
-- @param script
-- @param id of script
-- @param event Type of event
-- @param func Response function
function Machine:scriptAddEventFunc(script, id, event, func)
	assert(_broadcastdepth == 0, "Add script function not allowed during broadcast")
	local eventscripts = _eventscripttables[event]
	if not eventscripts then
		eventscripts = {}
		_eventscripttables[event] = eventscripts
	end

	eventscripts[script] = script
	script[event] = func
end

local function Pass()
end

--- Make a script stop responding to a type of event
-- @param script
-- @param id of script
-- @param event Type of event
function Machine:scriptRemoveEventFunc(script, id, event)
	local eventscripts = _eventscripttables[event]
	if not eventscripts then
		return
	end

	eventscripts[script] = nil
	script[event] = nil		-- clear func in script table
	if script[event] ~= nil then	-- still has func in class metatable
		script[event] = Pass
	end
end

--- Start a new instance of a script
-- @param id A key by which to reference the script instance
-- @param name Name of script class
-- @return The new script instance
function Machine:newScript(id, name, object, ...)
	assert(_broadcastdepth == 0, "Add script not allowed during broadcast")

	local script
	if name then
		local scriptclass = require(name)
			--self:requireScript(name)
		--_classes[name] = scriptclass
		script = scriptclass(object, ...)

		local idscripts = _idscriptarrays[id]
		if not idscripts then
			idscripts = {}
			_idscriptarrays[id] = idscripts
		end

		idscripts[#idscripts+1] = script

		local _eventscripttables = _eventscripttables
		for event, func in pairs(scriptclass) do
			if type(func) == "function" then
				local eventscripts = _eventscripttables[event]
				if not eventscripts then
					eventscripts = {}
					_eventscripttables[event] = eventscripts
				end
				eventscripts[script] = script
			end
		end
	end
	return script
end

--- Destroy all scripts belonging to an id
-- @param id
function Machine:destroyIdScripts(id)
	local idscripts = _idscriptarrays[id]
	if not idscripts then
		return
	end

	if _scriptlogs then
		for _, script in pairs(idscripts) do
			_scriptlogs[script] = nil
		end
	end

	for _, script in pairs(idscripts) do
		for _, eventscripts in pairs(_eventscripttables) do
			eventscripts[script] = nil
		end
	end
	_idscriptarrays[id] = nil
end

local function removeIdScript(idscripts, script)
	local i = pl_tablex_find(idscripts, script)
	if i then
		table_remove(idscripts, i)
	end
end

--- Destroy a script instance
-- @param script to be destroyed
-- @param id which owns the script
function Machine:destroyScript(script, id)
	if not script then
		return
	end
	if id then
		local idscripts = _idscriptarrays[id]
		if idscripts then
			removeIdScript(idscripts, script)
		end
	else
		for _, idscripts in pairs(_idscriptarrays) do
			removeIdScript(idscripts, script)
		end
	end
	for event, scripts in pairs(_eventscripttables) do
		scripts[script] = nil
	end
end

--- Find id's script that is of the given class or any subclass
-- @param id
-- @param scriptclass
function Machine:idHasScript(id, scriptclass)
	if type(scriptclass) == "string" then
		scriptclass = require(scriptclass)
	end
	local idscripts = _idscriptarrays[id]
	if not idscripts then
		return
	end
	for i = 1, #idscripts do
		local script = idscripts[i]
		if script:is_a(scriptclass) then
			return script
		end
	end
end

--- Call a function that returns a value on an id
-- As Machine:send, but on finding the first script that has the function,
-- stop and return the values returned by that function.
-- @param id that owns the script
-- @param event Type of event
-- @param ... Additional params
-- @return Up to 8 values returned from the called function
function Machine:call(id, event, ...)
	local list = _idscriptarrays[id]
	if not list then
		return
	end

	local a,b,c,d,e,f,g,h
	local i = 1
	while i <= #list do
		local script = list[i]
		local func = script[event]
		if func then
			if _scriptlogs then
				local log = _scriptlogs[script]
				if log then
					log[#log + 1] = { event, ... }
				end
			end
			a,b,c,d,e,f,g,h = func(script, ...)
			break
		end
		i = i + 1
	end

	return a,b,c,d,e,f,g,h
end

local function log(scripts, event, ...)
	for _, script in pairs(scripts) do
		local log = _scriptlogs[script]
		if log then
			log[#log + 1] = { event, ... }
		end
	end
end

--- Send event to one id's scripts
-- Calls a function on all of id's scripts which have that function.
-- Discards the return values.
-- @param id of script
-- @param event Type of event
-- @param ... Additional params
function Machine:send(id, event, ...)
	local array = _idscriptarrays[id]
	if not array then
		return
	end

	if _scriptlogs then
		log(array, event, ...)
	end

	local i = 1
	while i <= #array do
		local script = array[i]
		local func = script[event]
		if func then
			func(script, ...)
		end
		i = i + 1
	end
end
--[[
--- Metatable to catch new elements to table and put them in the temp space first
local ScriptTableMT = {}
function ScriptTableMT.__newindex(tbl, key, script)
	if script then
		local newscripts = _tablesnewscripts[tbl] or {}
		_tablesnewscripts[tbl] = newscripts
		newscripts[#newscripts+1] = key
		newscripts[#newscripts+1] = script
	else
		rawset(tbl, key, nil)
	end
end
]]
--- Send event to all interested scripts
-- @param event Type of event
-- @param ... Additional params
function Machine:broadcast(event, ...)
	local tbl = _eventscripttables[event]
	if not tbl then
		return
	end

	if _scriptlogs then
		log(tbl, event, ...)
	end

	_broadcastdepth = _broadcastdepth + 1

	for _, script in pairs(tbl) do
		script[event](script, ...)
	end

	_broadcastdepth = _broadcastdepth - 1
end

--- Start logging events for a script
-- @param id of script
function Machine:startLog(script)
	_scriptlogs = _scriptlogs or {}
	_scriptlogs[script] = {}
end

--- Print all events logged so far
function Machine:printLogs()
	if _scriptlogs then
		for script, log in pairs(_scriptlogs) do
			for i = 1, #log, 1 do
				print(unpack(log[i]))
			end
			if #log > 0 then
				print("--")
			end
		end
	end
end

--- Delete all events logged so far
function Machine:clearLogs()
	_scriptlogs = nil
end

local scripting = {
	newMachine = Machine,
	loaded = {}
}

local baseRequire = require

local function scriptRequire(name)
	if not package.loaded[name] then
		scripting.loaded[name] = scripting.loaded[name]
					or baseRequire(name)
	end
	return package.loaded[name]
end

function scripting.beginScriptLoading()
	baseRequire = require
	require = scriptRequire
end

function scripting.endScriptLoading()
	require = baseRequire
end

function scripting.unloadScripts()
	for n, _ in pairs(scripting.loaded) do
		package.loaded[n] = nil
		scripting.loaded[n] = nil
	end
end

return scripting
