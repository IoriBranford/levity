local type = type
local pairs = pairs
local ipairs = ipairs
local tonumber = tonumber

local math_floor = math.floor
local math_pi = math.pi
local math_rad = math.rad
local math_min = math.min
local string_sub = string.sub
local string_find = string.find

local love_data_decompress = love.data.decompress
local love_graphics_setCanvas = love.graphics.setCanvas
local love_graphics_clear = love.graphics.clear
local love_graphics_push = love.graphics.push
local love_graphics_pop = love.graphics.pop
local love_graphics_translate = love.graphics.translate
local love_graphics_scale = love.graphics.scale
local love_graphics_getWidth = love.graphics.getWidth
local love_graphics_getHeight = love.graphics.getHeight
local love_graphics_draw = love.graphics.draw
local love_graphics_points = love.graphics.points
local love_graphics_line = love.graphics.line
local love_graphics_circle = love.graphics.circle
local love_graphics_polygon = love.graphics.polygon
local love_graphics_newSpriteBatch = love.graphics.newSpriteBatch
local love_graphics_newParticleSystem = love.graphics.newParticleSystem

local prefs = require "levity.prefs"
local scripting = require "levity.scripting"
local Scripts_send = scripting.newMachine.send

local maputil = require "levity.maputil"
local maputil_setObjectsDefaultProperties = maputil.setObjectsDefaultProperties
local maputil_getAnimationUpdate = maputil.getAnimationUpdate

local sti = require "levity.sti.sti"
local Layer = require "levity.layer"
local Layer_init = Layer.init

local Object = require "levity.object"
local Object_init = Object.init
local Object_setLayer = Object.setLayer
local Object_freeBatchSprite = Object.freeBatchSprite

local Tiles = require "levity.tiles"
local Tiles_getUnflippedGid = Tiles.getUnflippedGid

local CanvasMaxScale = 4

--- @table Map
-- @field objecttypes
-- @field paused
-- @field animatedInstances

local Map = {
}
Map.__index = Map
-- Still want STI Map functions, so do not use class or metatable.

local TilesetMissingField = "Tileset %s has no %s named %s"

function Map.getTileGid(map, tilesetid, row, column)
	local tileset = map.tilesets[tilesetid]
	if not tileset then
		return nil
	end

	local tileid

	if not column then
		tileid = row
		if type(tileid) == "string" then
			tileid = tileset.namedtiles[tileid]
		elseif type(tileid) == "table" then
			local rowstype = tileset.properties.rowstype or "row"
			local colstype = tileset.properties.colstype or "column"
			row = tileid[rowstype] or 0
			column = tileid[colstype] or 0
		end
	end

	if column and row then
		if type(column) == "string" then
			local c = tileset.namedcols[column]
			if not c then
				return nil
				--error(TilesetMissingField:format(tileset.name, "column", column))
			end
			column = c
		end

		if type(row) == "string" then
			local r = tileset.namedrows[row]
			if not r then
				return nil
				--error(TilesetMissingField:format(tileset.name, "row", row))
			end
			row = r
		end

		tileid = row * tileset.tilecolumns + column
	end

	return tileid and tileset.firstgid + tileid
end

function Map.getTileRowName(map, gid)
	local tileset = map.tilesets[map.tiles[gid].tileset]
	local tileid = gid - tileset.firstgid
	local row = tileid / tileset.tilecolumns
	return tileset.rownames[row] or math_floor(row)
end

function Map.getTileColumnName(map, gid)
	local tileset = map.tilesets[map.tiles[gid].tileset]
	local tileid = gid - tileset.firstgid
	local column = tileid % tileset.tilecolumns
	return tileset.columnnames[column] or column
end

function Map.getTileLineName(map, gid, linetype)
	local tileset = map.tilesets[map.tiles[gid].tileset]
	local tileid = gid - tileset.firstgid
	if linetype == tileset.properties.colstype then
		local column = tileid % tileset.tilecolumns
		return tileset.columnnames[column] or column
	elseif linetype == tileset.properties.rowstype then
		local row = math_floor(tileid / tileset.tilecolumns)
		return tileset.rownames[row] or row
	end
end

function Map.getTileset(map, tilesetid)
	return map.tilesets[tilesetid]
end

function Map.getTile(map, tilesetid, tileid)
	return map.tiles[map:getTileGid(tilesetid, tileid)]
end

function Map.getTilesetImage(map, tilesetid)
	return map.tilesets[tilesetid].image
end

function Map.getTileShapePosition(map, gid, shapeobj, center)
	local tile = map.tiles[gid]
	if type(shapeobj) == "string" then
		local objectGroup = tile.objectGroup
		if not objectGroup then
			return
		end
		shapeobj = objectGroup.objects[shapeobj]
	end
	if not shapeobj then
		return
	end

	local tileoffsetx = tile.offset.x
	local tileoffsety = tile.offset.y
	local tilesx = tile.sx
	local tilesy = tile.sy
	local tilewidth = tile.width
	local tileheight = tile.height

	local x = shapeobj.x
	local y = shapeobj.y
	local w = shapeobj.width
	local h = shapeobj.height

	--local cx = x + w/2
	--local cy = y + h/2
	--local tilecentertocx = cx - tilewidth/2
	--local tilecentertocy = cy - tileheight/2
	--cx = cx + tilecentertocx*(1 - tilesx)
	--cy = cy + tilecentertocy*(1 - tilesy)
	--x = cx - w/2
	--y = cy - h/2

	x = tilesx*x + (1 - tilesx)*(tilewidth - w)/2
	y = tilesy*y + (1 - tilesy)*(tileheight - h)/2

	local shapepointx = 0
	local shapepointy = 0
	if center then
		shapepointx = w/2
		shapepointy = h/2
	end
	x = x + shapepointx
	y = y + shapepointy

	return x + tileoffsetx, y + tileoffsety - tileheight
end

--- Convert list of map-specific gids to map-agnostic names
-- @param gids list
-- @return List of name tables: { {tileset, row, column}, ... }
function Map.tileGidsToNames(map, gids)
	if not gids then
		return nil
	end
	local names = {}
	local tiles = map.tiles
	for _, gid in ipairs(gids) do
		local tile = tiles[gid]
		local tileset = map.tilesets[tile.tileset]
		local name = tile.properties and tile.properties.name
		names[#names + 1] = {
			tileset = tileset.name,
			tile = name,
			row = map:getTileRowName(gid),
			column = map:getTileColumnName(gid)
		}
	end
	return names
end

--- Convert name tables to gids for current map
-- @param names list returned by tileGidsToNames
-- @return List of tile gids
function Map.tileNamesToGids(map, names)
	if not names then
		return nil
	end
	local gids = {}
	for _, name in ipairs(names) do
		local gid
		if name.tile then
			gid = map:getTileGid(name.tileset, name.tile)
		else
			gid = map:getTileGid(name.tileset, name.row, name.column)
		end
		gids[#gids + 1] = gid
	end
	return gids
end

function Map.newSpriteBatch(map, tileset, size, usage)
	size = size or 32
	usage = usage or "dynamic"
	local tileset = map.tilesets[tileset]
	local image = tileset.image

	local spritebatch = love_graphics_newSpriteBatch(image, size, usage)
	return spritebatch
end

function Map.addBatchSprite(map, batch, gid, x, y, r, sx, sy, ox, oy, kx, ky)
	local tile = map.tiles[gid]
	local quad = tile.quad
	return batch:add(quad, x, y, r, sx, sy,
			(ox or 0) - tile.offset.x,
			(oy or 0) - tile.offset.y + tile.height,
			kx, ky)
end

function Map.setBatchSprite(map, batch, i, gid, x, y, r, sx, sy, ox, oy, kx, ky)
	if not gid or gid <= 0 then
		batch:set(i, 0, 0, 0, 0, 0)
	else
		local tile = map.tiles[gid]
		local quad = tile.quad
		batch:set(i, quad, x, y, r, sx, sy,
			(ox or 0) - tile.offset.x,
			(oy or 0) - tile.offset.y + tile.height,
			kx, ky)
	end
end

function Map.newParticleSystem(map, gid, buffersize)
	buffersize = buffersize or 256
	local tile = map.tiles[gid]
	local tilesetid = tile.tileset
	local tileset = map.tilesets[tilesetid]
	local image = tileset.image

	local particles = love_graphics_newParticleSystem(image, buffersize)

	particles:setParticleLifetime(1)
	particles:setEmissionRate(0)
	particles:setSpread(2*math_pi)
	particles:setSpeed(60)
	map:setParticlesGid(particles, gid)
	return particles
end

local function cacheAnimQuads(animation, tiles, firstgid)
	local quads = {}
	for i = 1, #animation do
		local frame = animation[i]
		local frametile = tiles[firstgid + frame.tileid]
		quads[#quads+1] = frametile.quad
	end
	animation.quads = quads
	return quads
end

function Map.setParticlesGid(map, particles, gid)
	local tile = map.tiles[gid]
	local tilesetid = tile.tileset
	local tileset = map.tilesets[tilesetid]
	particles:setTexture(tileset.image)

	local animation = tile.animation
	if animation then
		local firstgid = tileset.firstgid

		local quads = animation.quads
			or cacheAnimQuads(animation, map.tiles, firstgid)
		particles:setQuads(unpack(quads))
	else
		particles:setQuads(tile.quad)
	end
end

function Map.getAnimTime(map, gid)
	local ms = 0
	local tile = map.tiles[gid]
	local animation = tile and tile.animation
	if animation then
		for i = 1, #animation do
			ms = ms + animation[i].duration
		end
	end
	return ms/1000
end

function Map.newObject(map, layer)
	local freeobjects = map.freeobjects
	local object = freeobjects[#freeobjects]
	if object then
		freeobjects[#freeobjects] = nil
		for k,_ in pairs(object.properties) do
			object[k] = nil
		end
	else
		local id = map.nextobjectid
		object = { id = id, properties = { } }
		map.nextobjectid = id + 1
	end
	if layer then
		if type(layer) ~= "table" then
			layer = map.layers[layer]
		end
		layer:addObject(object)
	end
	return object
end

function Map.cleanupObjects(map, discardedobjects)
	local objects = map.objects
	--local freeobjects = map.freeobjects
	for id, object in pairs(discardedobjects) do
		Object_freeBatchSprite(object)
		Object_setLayer(object, nil)

		if object.body then
			object.body:setActive(false)
			for _, fixture in pairs(object.body:getFixtures()) do
				fixture:setUserData(nil) -- work around https://bitbucket.org/rude/love/issues/1273
				fixture:destroy()
			end
			object.body:destroy()
			object.body = nil
		end
		local properties = object.properties
		for k,v in pairs(properties) do
			properties[k] = nil
		end
		for k,v in pairs(object) do
			object[k] = nil
		end
		object.id = id
		object.properties = properties
		objects[id] = nil
		-- Disabled returning objects to the free pool
		-- to fix issues with same id referring to a new object
		--freeobjects[#freeobjects+1] = object
	end
end

function Map.update(map, dt, scripts)
	local tiles = map.tiles
	local tilesets = map.tilesets
	for gid, instances in pairs(map.animatedInstances) do
		local tile = tiles[gid]
		local animation = tile.animation
		local frame = tile.frame
		local time = tile.time
		frame, tile.time = maputil_getAnimationUpdate(animation, frame, time, dt)

		if frame ~= tile.frame then
			tile.frame = frame
			local tileset = tilesets[tile.tileset]
			local framegid = animation[frame].tileid + tileset.firstgid
			local frametile = tiles[framegid]
			local framequad = frametile.quad
			local tilesx = tile.sx
			local tilesy = tile.sy
			for _, j in pairs(instances) do
				if j.layer.properties.animated ~= false then
					j.batch:set(j.id, framequad, j.x, j.y,
						j.r, tilesx, tilesy, 0, j.oy)
				end
			end
		end
	end

	for _, layer in ipairs(map.layers) do
		layer:update(dt, map, scripts)
	end
end

local VisibleFixtures = {}

function Map.draw(map, camera, scripts, world)
	local r, g, b = 0, 0, 0
	local backgroundcolor = map.backgroundcolor
	if backgroundcolor then
		r, g, b = backgroundcolor[1], backgroundcolor[2], backgroundcolor[3]
		r = (1+r)/256
		g = (1+g)/256
		b = (1+b)/256
	end
	local canvas = map.canvas
	if canvas then
		love_graphics_setCanvas(canvas)
	end
	love_graphics_clear(r, g, b)

	local cx, cy = camera.x, camera.y
	local cw, ch = camera.w, camera.h
	local ccx, ccy = cx+cw*.5, cy+ch*.5

	local scale = camera.scale
	local intscale = camera.intscale

	love_graphics_push()
	love_graphics_scale(intscale, intscale)
	love_graphics_translate(-cx, -cy)

	if scripts then
		Scripts_send(scripts, map.name, "beginDraw")
	end
	for _, layer in ipairs(map.layers) do
		local rendertarget = layer.properties.setrendertarget
		if canvas then
			if rendertarget=="canvas" then
				love_graphics_setCanvas(canvas)
			elseif rendertarget=="screen" then
				love_graphics_setCanvas()
			end
		end

		if layer.visible and layer.opacity > 0 then
			if scripts then
				Scripts_send(scripts, layer.name, "beginDraw")
			end

			love_graphics_push()
			love_graphics_translate(layer.offsetx, layer.offsety)

			local r,g,b,a = love.graphics.getColor()
			love.graphics.setColor(r, g, b, a * layer.opacity)

			if scripts then
				Scripts_send(scripts, layer.name, "drawUnder")
			end

			layer:draw(camera, scripts, map)

			if scripts then
				Scripts_send(scripts, layer.name, "drawOver")
			end

			love.graphics.setColor(r,g,b,a)

			love_graphics_pop()

			if scripts then
				Scripts_send(scripts, layer.name, "endDraw")
			end
		end
	end
	if scripts then
		Scripts_send(scripts, map.name, "endDraw")
	end

	if world then
		world:queryBoundingBox(cx, cy, cx+cw, cy+ch,
			function(fixture)
				VisibleFixtures[#VisibleFixtures+1] = fixture
				return true
			end)

		for _, fixture in ipairs(VisibleFixtures) do
			local body = fixture:getBody()
			love_graphics_circle("line", body:getX(), body:getY(),
						intscale)

			local bodycx, bodycy = body:getWorldCenter()
			love_graphics_line(bodycx - intscale, bodycy,
					bodycx + intscale, bodycy)
			love_graphics_line(bodycx, bodycy - intscale,
					bodycx, bodycy + intscale)

			local shape = fixture:getShape()
			if shape:getType() == "circle" then
				local x, y = body:getWorldPoint(
					shape:getPoint())
				love_graphics_circle("line", x, y,
					shape:getRadius())
				love_graphics_points(x, y)
			elseif shape:getType() == "polygon" then
				love_graphics_polygon("line",
					body:getWorldPoints(shape:getPoints()))
			elseif shape:getType() == "chain" then
				love_graphics_line(
					body:getWorldPoints(shape:getPoints()))
			end
		end

		while #VisibleFixtures > 0 do
			VisibleFixtures[#VisibleFixtures] = nil
		end
	end

	love_graphics_pop()

	if canvas then
		love_graphics_setCanvas()
		local canvasscale = scale / intscale
		love_graphics_draw(canvas,
					love_graphics_getWidth()*.5,
					love_graphics_getHeight()*.5,
					math_rad(prefs.rotation),
					canvasscale, canvasscale,
					canvas:getWidth()*.5,
					canvas:getHeight()*.5)
	end
end

local function initTileset(tileset, tiles)
	tileset.tilecolumns =
		math_floor(tileset.imagewidth / tileset.tilewidth)

	tileset.namedtiles = {}
	tileset.namedrows = {}
	tileset.namedcols = {}
	tileset.rownames = {}
	tileset.columnnames = {}
	--tileset.tilenames = {}

	for p, v in pairs(tileset.properties) do
		if string_find(p, "rowname") == 1 then
			local num = tonumber(string_sub(p, 8))
			tileset.rownames[num] = v
			tileset.namedrows[v] = num
		elseif string_find(p, "colname") == 1 then
			local num = tonumber(string_sub(p, 8))
			tileset.columnnames[num] = v
			tileset.namedcols[v] = num
		elseif string_find(p, "row_") == 1 then
			local name = string_sub(p, 5)
			tileset.rownames[v] = name
			tileset.namedrows[name] = v
		elseif string_find(p, "column_") == 1 then
			local name = string_sub(p, 8)
			tileset.columnnames[v] = name
			tileset.namedcols[name] = v
		end
	end

	local firstgid = tileset.firstgid
	local lastgid = firstgid + tileset.tilecount - 1

	local tilesettiles = tileset.tiles
	for j = 1, #tilesettiles do
		local tile = tilesettiles[j]
		if tile.properties then
			local name = tile.properties.name
			if name then
				--tileset.tilenames[tile.id] = name
				tileset.namedtiles[name] = tile.id
				tilesettiles[name] = tiles[firstgid + tile.id]
			end
		end
		if tile.objectGroup then
			local objects = tile.objectGroup.objects
			for _, object in pairs(objects) do
				local name = object.name
				if name ~= "" then
					objects[name] = object
				end
			end
		end

		local animation = tile.animation
		if animation then
			local duration = 0
			for i = 1, #animation do
				local frame = animation[i]
				duration = duration + frame.duration
			end
			animation.duration = duration

			if tile.properties and tile.properties.cacheanimquads then
				cacheAnimQuads(animation, tiles, firstgid)
			end
		end
	end

	local commonanimation = tileset.properties.commonanimation

	if commonanimation then
		if type(commonanimation) == "string" then
			commonanimation = tileset.namedtiles[commonanimation]
		end
		local commonanimationid = commonanimation
		local commonanimationtilegid =
			firstgid + commonanimationid
		local commonanimationtile =
			tiles[commonanimationtilegid]

		commonanimation = commonanimationtile.animation

		for i = firstgid, lastgid do
			local tile = tiles[i]
			if not tile.animation then
				local animation = {}
				animation.duration = commonanimation.duration

				for _, frame in ipairs(commonanimation) do
					local tileid = tile.id - commonanimationid + (frame.tileid)

					table.insert(animation, {
						tileid = tileid,
						duration = frame.duration
					})
				end

				if tile.properties.cacheanimquads then
					cacheAnimQuads(animation, tiles, firstgid)
				end

				tile.animation = animation
			end
		end
	end

	local commoncollision = tileset.properties.commoncollision

	if commoncollision then
		if type(commoncollision) == "string" then
			commoncollision = tileset.namedtiles[commoncollision]
		end
		local commoncollisiontilegid =
			firstgid + commoncollision
		local commoncollisiontile =
			tiles[commoncollisiontilegid]

		commoncollision = commoncollisiontile.objectGroup

		for i = firstgid, lastgid do
			local tile = tiles[i]
			tile.objectGroup = tile.objectGroup or commoncollision
		end
	end
end

function Map.initScripts(map, scripts)
	local layers = map.layers
	for i = 1, #layers do
		local layer = layers[i]

		if layer.objects then
			for _, object in pairs(layer.objects) do
				local script = object.properties.script
				if script then
					require(script)
				end
			end

			if layer.type == "dynamiclayer"
			and not map.properties.delayinitobjects then
				for _, object in pairs(layer.objects) do
					Object_init(object, layer, map)
				end
			end
		end

		scripts:newScript(layer.name, layer.properties.script, layer)
	end

	scripts:newScript(map.name, map.properties.script, map)

	for _, object in pairs(map.objects) do
		Scripts_send(scripts, object.id, "initQuery")
	end
	for i = 1, #layers do
		Scripts_send(scripts, layers[i].name, "initQuery")
	end
	Scripts_send(scripts, map.name, "initQuery")
end

function Map.windowResized(map, w, h, camera)
	camera:updateScale()
	local intscale = camera.intscale
	map:resize(camera.w * intscale, camera.h * intscale)
	local filter = prefs.canvasscalesoft and "linear" or "nearest"
	map.canvas:setFilter(filter, filter)
end

local function incIdProperties(properties, incid)
	for pn, pv in pairs(properties) do
		if type(pv) == "number" and string_sub(pn, -2) == "id" then
			properties[pn] = pv + incid
		end
	end
end

local function mergeMaps(map1, map2)
	--Bump gids
	local lasttileset1 = map1.tilesets[#map1.tilesets]
	local incgid = lasttileset1.firstgid + lasttileset1.tilecount - 1

	--Bump object ids
	local incid = map1.nextobjectid - 1

	--Next object id
	map1.nextobjectid = incid + map2.nextobjectid

	local tilesets1 = map1.tilesets
	local tilesets2 = map2.tilesets
	for i = 1, #tilesets2 do
		local tileset2 = tilesets2[i]
		--Tileset firstgids
		tileset2.firstgid = tileset2.firstgid + incgid
		tilesets1[#tilesets1 + 1] = tileset2
	end

	--Object id references in properties
	incIdProperties(map2.properties, incid)
	for pn, pv in pairs(map2.properties) do
		if map1.properties[pn] == nil then
			map1.properties[pn] = pv
		end
	end

	local layers1 = map1.layers
	local layers2 = map2.layers
	for i = 1, #layers2 do
		local layer2 = layers2[i]

		--Object id references in properties
		incIdProperties(layer2.properties, incid)

		if layer2.objects then
			for _, object2 in pairs(layer2.objects) do
				--Tile object gids
				if object2.gid then
					object2.gid = object2.gid + incgid
				end

				--Object ids
				object2.id = object2.id + incid

				--Object id references in properties
				incIdProperties(object2.properties, incid)
			end
		end

		--Tile layer gids
		local data = layer2.data

		if layer2.encoding == "base64" then
			local utils = require "sti.sti.utils"
			require "ffi"

			local fd  = love.filesystem.newFileData(data, "data",
							"base64"):getString()

			local compression = layer2.compression
			if compression == "zlib" or compression == "gzip" then
				fd = love_data_decompress(fd, compression)
			end

			data = utils.get_decompressed_data(fd)

			layer2.data = data
			layer2.encoding = nil
		end

		if data then
			for i = 1, #data do
				data[i] = data[i] + incgid
			end
		end

		layers1[#layers1 + 1] = layer2
	end

	return map1
end

function Map.loadFonts(map, fonts)
	local layers = map.layers
	for l = 1, #layers do
		local objects = layers[l].objects
		if objects then
			for _, object in pairs(objects) do
				local font = object.fontfamily
				local fontsize = object.pixelsize or 16
				local bold = object.bold
				local italic = object.italic
				local fonttype = object.properties.fonttype or "fnt"
				if font then
					fonts:load(font, fontsize, bold, italic, fonttype)
				end
			end
		end
	end
end

function Map.loadObjectSounds(map, object, bank)
	map:loadSoundsFromProperties(object.properties, bank)
	map:loadObjectTypeSounds(object.type, bank)
end

function Map.loadObjectTypeSounds(map, typ, bank)
	local objecttypes = map.objecttypes
	local objecttype = objecttypes and objecttypes[typ]
	if objecttype then
		map:loadSoundsFromProperties(objecttype, bank)
		map:loadObjectTypeSounds(objecttype._basetype, bank)
	end
end

function Map.loadSoundsFromProperties(map, properties, bank)
	for name, value in pairs(properties) do
		if type(value) == "string" then
			if bank.isAudioFile(value) then
				bank:load(value, "static")
			elseif bank.isMusicEmuFile(value) then
				bank:load(value, "stream")
			end
		end
	end
end

function Map.loadSounds(map, bank)
	local layers = map.layers
	for i = 1, #layers do
		local layer = layers[i]
		local objects = layer.objects
		if objects then
			for i = 1, #objects do
				map:loadObjectSounds(objects[i], bank)
			end
		end
		map:loadSoundsFromProperties(layer.properties, bank)
	end
	map:loadSoundsFromProperties(map.properties, bank)
	local tiles = map.tiles
	for i = 1, #tiles do
		map:loadObjectSounds(tiles[i], bank)
	end
end

local function objectIdsToInts(properties)
	for k, v in pairs(properties) do
		if type(v) == "table" then
			properties[k] = v.id
		end
	end
end

function Map.loadTileset(map, file)
	local tileset = love.filesystem.load(file)()
	local dir = file:reverse():find("[/\\]") or ""
	if dir ~= "" then
		dir = file:sub(1, 1 + (#file - dir))
	end
	map:addTileset(tileset, dir)
	map.tilesets[tileset.name] = tileset
	initTileset(tileset, map.tiles)

	for i = tileset.firstgid, tileset.firstgid + tileset.tilecount - 1 do
		local tile = map.tiles[i]
		tile.image = tileset.image
	end

	local objecttypes = map.objecttypes
	if objecttypes then
		for i = tileset.firstgid, tileset.firstgid + tileset.tilecount - 1 do
			local tile = map.tiles[i]
			local objectgroup = tile.objectGroup
			if objectgroup then
				maputil_setObjectsDefaultProperties(
					objectgroup.objects, objecttypes)
			end

			maputil.setObjectDefaultProperties(tile, objecttypes)
		end
	end

	return tileset
end

local function newMap(mapfile, newmegatileset)
	local map = love.filesystem.load(mapfile)()
	for i, layer in ipairs(map.layers) do
		layer.mapfile = mapfile
	end
	local overlaymap = map.properties.overlaymap
	if overlaymap then
		local map2 = love.filesystem.load(overlaymap)()
		for i, layer in ipairs(map2.layers) do
			layer.mapfile = overlaymap
		end
		map = mergeMaps(map, map2)
	end

	for _, layer in ipairs(map.layers) do
		if layer.objects and layer.properties.static ~= true then
			layer.type = "dynamiclayer"
		end
	end

	local tilesets = map.tilesets

	local mapinfo = love.filesystem.getInfo(mapfile)
	local megatilesetpath = "megatileset_"..mapfile
	local megaimagepath = megatilesetpath..".png"
	local megatilesetinfo = love.filesystem.getInfo(megatilesetpath)
	local mapmodtime = mapinfo.modtime or 0
	local megatilesetmodtime = megatilesetinfo and megatilesetinfo.modtime or 0
	newmegatileset = newmegatileset or megatilesetmodtime < mapmodtime

	if not newmegatileset then
		for i = 1, #tilesets do
			tilesets[i].image = megaimagepath
		end
	end

	map = sti(map, {"box2d", "megatileset"})
	for fname, f in pairs(Map) do
		if fname ~= "__call" then
			map[fname] = f
		end
	end

	local result, err
	if newmegatileset then
		result, err = map:megatileset_init()
		if result then
			map:megatileset_save(megatilesetpath, megaimagepath, result)
		end
	else
		result, err = map:megatileset_load(megatilesetpath)
	end
	if not result then
		print(err)
	end

	map.name = mapfile
	map.paused = false

	local tiles = map.tiles
	local width = map.width * map.tilewidth
	local height = map.height * map.tileheight

	local objecttypes = maputil.loadObjectTypesFile(
		map.properties.objecttypesxml or "objecttypes.xml")
	map.objecttypes = objecttypes

	if objecttypes then
		maputil.setObjectTypesBases(map.objecttypes)

		for _, layer in ipairs(map.layers) do
			if layer.objects then
				maputil_setObjectsDefaultProperties(
					layer.objects, objecttypes)
			end
		end

		for t, properties in pairs(objecttypes) do
			for k, v in pairs(properties) do
				if string_sub(k, -2) == "id"
				and (v == 0 or v == "") then
					properties[k] = nil
				end
			end
		end

		for i = 1, #tiles do
			local tile = tiles[i]
			local objectgroup = tile.objectGroup
			if objectgroup then
				maputil_setObjectsDefaultProperties(
					objectgroup.objects, objecttypes)
			end
		end

		maputil_setObjectsDefaultProperties(tiles, objecttypes)
	end

	for _, tileset in ipairs(map.tilesets) do
		tilesets[tileset.name] = tileset
		initTileset(tileset, tiles)
	end

	for gid, tile in pairs(tiles) do
		local tileset = tilesets[tile.tileset]
		tile.image = tileset.image
	end

	setmetatable(tiles, {
		__index = function(tiles, gid)
			if not gid then
				return
			end
			local unflippedgid = Tiles_getUnflippedGid(gid)
			local unflippedtile = rawget(tiles, unflippedgid)
			if not unflippedtile then
				error("There is no tile with base gid "
				..unflippedgid .." (out of "..#tiles.." tiles)")
			end
			local tile = map:setFlippedGID(gid)
			tile.image = map.tilesets[tile.tileset].image
			tile.objectGroup = unflippedtile.objectGroup
			return tile
		end
	})

	local setObjectCoordinates = map.setObjectCoordinates
	local objects = map.objects
	for _, layer in ipairs(map.layers) do
		if layer.type == "dynamiclayer" then
			setObjectCoordinates(map, layer)
			Layer_init(layer)
			for _, object in pairs(layer.objects) do
				objects[object.id] = object
				local gid = object.gid
				if gid then
					object.tile = tiles[gid]
				end
				local r = object.rotation
				if r then
					object.rotation = math_rad(r)
				end
			end
		elseif layer.type == "tilelayer" or layer.type == "imagelayer"
		then
			Layer_init(layer)
		end
		objectIdsToInts(layer.properties)
		if layer.objects then
			for _, object in pairs(layer.objects) do
				objectIdsToInts(object.properties)
			end
		end
	end
	objectIdsToInts(map.properties)

	local animatedInstances = {}
	map.animatedInstances = animatedInstances
	for gid, instances in pairs(map.tileInstances) do
		local tile = tiles[gid] or map:setFlippedGID(gid)
		if tile.animation then
			animatedInstances[gid] = instances
		end
	end

	local freeobjects = {}
	map.freeobjects = freeobjects
	for i = map.nextobjectid-1, 1, -1 do
		if not objects[i] then
			freeobjects[#freeobjects+1] = { id = i, properties = { } }
		end
	end

	return map
end

return newMap
