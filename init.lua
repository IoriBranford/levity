love.filesystem.setRequirePath(
	"levity/pl/lua/?.lua;"..
	"levity/pl/lua/?/init.lua;"..
	love.filesystem.getRequirePath())

require("pl.strict").module("_G", _G)
class = require "pl.class"

local tonumber = tonumber
local collectgarbage = collectgarbage

local love_graphics_setFont = love.graphics.setFont
local love_graphics_getFont = love.graphics.getFont
local love_graphics_setNewFont = love.graphics.setNewFont
local love_graphics_clear = love.graphics.clear
local love_graphics_printf = love.graphics.printf
local love_graphics_getWidth = love.graphics.getWidth
local love_graphics_getHeight = love.graphics.getHeight

local audio = require "levity.audio"
local Bank_update = audio.newBank.update

local text = require "levity.text"
local stats = require "levity.stats"

local scripting = require "levity.scripting"
local Scripts_broadcast = scripting.newMachine.broadcast
local Scripts_send = scripting.newMachine.send
local Scripts_destroyIdScripts = scripting.newMachine.destroyIdScripts
local Scripts_clearLogs = scripting.newMachine.clearLogs
local Scripts_printLogs = scripting.newMachine.printLogs

local sti = require "levity.sti.sti"

local Map = require "levity.map"

local profile

require "levity.xcoroutine"
require "levity.xmath"

local hassteam, steam = pcall(require, "luasteam")
if not hassteam then
	steam = nil
end

---
-- @field map
-- @field mapfile
-- @field camera
-- @field scripts
-- @field world
-- @field collisionrules
-- @field discardedobjects
-- @field bank
-- @field fonts
-- @field prefs
-- @field stats
-- @field timescale
-- @field maxdt
-- @field movetimer Time left until next move
-- @field movedt Time between each move
-- @field nextmapfile Will load and switch to this map on the next frame
-- @field nextmapdata
-- @field systemfont
-- @field systemfontsize
-- @field version
-- @field showversion
-- @field lastinputdevice
-- @table levity

local levity = {
	prefs = require "levity.prefs"
}
local levity_scripts

function levity:setNextMap(nextmapfile, nextmapdata)
	self.nextmapfile = nextmapfile
	self.nextmapdata = nextmapdata
end

local function collisionEvent(self, event, fixture, ...)
	local ud = fixture:getBody():getUserData()
	local id = ud and ud.id or self.mapfile
	if id then
		Scripts_send(levity_scripts, id, event, fixture, ...)
	end
end

local function initPhysics(self)
	self.world = love.physics.newWorld(0, self.map.properties.gravity or 0)

	local function beginContact(fixture1, fixture2, contact)
		local nx, ny = contact:getNormal()
		collisionEvent(self, "beginContact", fixture1, fixture2, contact, nx, ny)
		collisionEvent(self, "beginContact", fixture2, fixture1, contact, -nx, -ny)
	end

	local function endContact(fixture1, fixture2, contact)
		local nx, ny = contact:getNormal()
		collisionEvent(self, "endContact", fixture1, fixture2, contact, nx, ny)
		collisionEvent(self, "endContact", fixture2, fixture1, contact, -nx, -ny)
	end

	local function preSolve(fixture1, fixture2, contact)
		local nx, ny = contact:getNormal()
		collisionEvent(self, "preSolve", fixture1, fixture2, contact, nx, ny)
		collisionEvent(self, "preSolve", fixture2, fixture1, contact, -nx, -ny)
	end

	local function postSolve(fixture1, fixture2, contact,
				normal1, tangent1, normal2, tangent2)
		local nx, ny = contact:getNormal()
		collisionEvent(self, "postSolve", fixture1, fixture2, contact, nx, ny,
				normal1, tangent1, normal2, tangent2)
		collisionEvent(self, "postSolve", fixture2, fixture1, contact, -nx, -ny,
				normal1, tangent1, normal2, tangent2)
	end

	self.world:setCallbacks(beginContact, endContact, preSolve, postSolve)

	local objecttypes = levity.map.objecttypes

	-- box2d plugin will deep-copy tile shape objects, losing the metatables
	-- copy properties from objecttypes first to work around this
	for _, tile in pairs(levity.map.tiles) do
		local objectGroup = tile.objectGroup
		if objectGroup then
			for _, object in ipairs(objectGroup.objects) do
				local typ = object.type
				local properties = object.properties
				typ = objecttypes[typ]
				if typ then
					for k,v in pairs(typ) do
						if rawget(properties, k) == nil then
							properties[k] = v
						end
					end
				end
			end
		end
	end

	self.map:box2d_init(self.world)

	for _, fixture in pairs(self.map.box2d_collision.body:getFixtures()) do
		local fixturedata = fixture:getUserData()
		local fixtureproperties = fixturedata.properties
		local category = fixtureproperties.category

		if category then
			if type(category) == "string" then
				category = self.collisionrules["Category_"..category]
			end
			fixture:setCategory(category)
		end
	end
end

local function camera_set(camera, cx, cy, w, h)
	if w then
		camera.w = w
	end
	if h then
		camera.h = h
	end
	if w or h then
		camera:updateScale()
	end
	camera.x = (cx - camera.w * .5)
	camera.y = (cy - camera.h * .5)
end

local function camera_updateScale(camera)
	local r = math.rad(levity.prefs.rotation)
	local cosr = math.abs(math.cos(r))
	local sinr = math.abs(math.sin(r))
	local gw = love_graphics_getWidth()
	local gh = love_graphics_getHeight()
	local sw = gw*cosr + gh*sinr
	local sh = gh*cosr + gw*sinr
	local scale = math.min(sw/camera.w, sh/camera.h)
	if levity.prefs.canvasscaleint then
		scale = math.floor(scale)
	end
	camera.scale = scale
	camera.intscale = levity.prefs.canvasresolution == "HIGH" and math.floor(scale) or 1
end

local function camera_zoom(camera, vz)
	local aspect = camera.w / camera.h
	camera:set(camera.x - vz*aspect*.5,
		camera.y - vz*.5,
		camera.w + vz*aspect,
		camera.h + vz)
end

local function applyResize(w, h)
	levity.map:windowResized(w, h, levity.camera)
	levity_scripts:broadcast("resize", w, h)
	if levity.loadBackground then
		levity.loadBackground()
	end
end

function levity:loadNextMap()
	love.audio.stop()

	self.scripts = scripting.newMachine()
	levity_scripts = self.scripts

	scripting.unloadScripts()
	scripting.endScriptLoading()

	self.mapfile = self.nextmapfile
	self.nextmapfile = nil

	if self.map then
		self:cleanupObjects(self.map.objects)
		sti:flush()
	end

	if self.world then
		self.world:setCallbacks(nil, nil, nil, nil)
		self.world:destroy()
	end

	self.bank = audio.newBank()
	self.fonts = text.newFonts()
	self.stats = stats.newStats()
	self.map = nil
	self.discardedobjects = {}
	self.camera = {
		x = 0, y = 0,
		w = love_graphics_getWidth(), h = love.graphics.getHeight(),
		scale = 1,
		r = 0,
		set = camera_set,
		zoom = camera_zoom,
		updateScale = camera_updateScale,
	}

	collectgarbage()

	self.map = Map(self.mapfile)
	self.map:loadFonts(self.fonts)
	self.map:loadSounds(self.bank)

	initPhysics(self)

	scripting.beginScriptLoading()
	self.map:initScripts(levity_scripts)

	applyResize(love_graphics_getWidth(), love_graphics_getHeight())
	-- After initScripts because script is where camera size is set.

	self.timescale = 1
	self.movetimer = 0
	self.movedt = 1/60
	self.maxdt = self.movedt
	collectgarbage()
end

function levity:getObject(id)
	return self.discardedobjects[id] == nil and self.map.objects[id]
end

function levity:discardObject(id)
	self.discardedobjects[id] = self.map.objects[id]
end

function levity:cleanupObjects(discardedobjects)
	self.map:cleanupObjects(discardedobjects)

	for id, _ in pairs(discardedobjects) do
		Scripts_destroyIdScripts(levity_scripts, id)
		discardedobjects[id] = nil
	end
end
local levity_cleanupObjects = levity.cleanupObjects

function levity:screenToCamera(x, y)
	local rotation = math.rad(levity.prefs.rotation)
	local cosr = math.cos(rotation)
	local sinr = math.sin(rotation)
	local scale = self.camera.scale
	x = x - love_graphics_getWidth() *.5
	y = y - love_graphics_getHeight()*.5
	x = x / scale
	y = y / scale
	x, y = x*cosr + y*sinr, -x*sinr + y*cosr
	x = x + self.camera.w*.5
	y = y + self.camera.h*.5
	return x, y
end

local function parseARGB(argb)
	local ARGBFormat = "#(%x%x)(%x%x)(%x%x)(%x%x)"
	local a, r, g, b = argb:match(ARGBFormat)
	a, r, g, b = tonumber(a, 16), tonumber(r, 16), tonumber(g, 16), tonumber(b, 16)
	a = (1 + a)/256
	r = (1 + r)/256
	g = (1 + g)/256
	b = (1 + b)/256
	return a, r, g, b
end

local function setColorARGB(argb)
	if not argb then
		return
	end
	local a, r, g, b = parseARGB(argb)
	love.graphics.setColor(r, g, b, a)
end

levity.parseARGB = parseARGB
levity.setColorARGB = setColorARGB

function levity:timerCorrectRoundingError(timer, time)
	local diff = math.abs(timer - time)
	if diff < self.movedt*.5 then
		timer = time
	end
	return timer
end

function levity:setSystemFont(systemfont, systemfontsize)
	levity.systemfont = systemfont
	levity.systemfontsize = systemfontsize
end

local Usage = {
	Desc =	"Levity 2D game engine\n",
	Console="  --console				Output to a console window\n",
	Version="  --version				Print LOVE version\n",
	Fused =	"  --fused				Force running in fused mode\n",
	Game =	"  <game>	(string)		Game assets location\n",
	Debug =	"  -debug				Debug in Zerobrane Studio\n",
	Prefs = [[
  --windowed				Start in windowed mode
  --rotation	(number default -1)	Screen orientation in degrees clockwise
  --drawstats				Draw performance stats
  --drawbodies				Draw physical bodies
  --profile				Profile code performance
  --exhibit				Exhibit mode - disable options menu and quit
  --exclusive				Exclusive fullscreen
  --buildmegatilesets	(optional string)	Build megatilesets for all maps in the given text file
]],
	Map =	"  <map>	(string default %s)	Map file to start\n",
}

local function showMessageBox_joystickNeedsMapping(joystick)
	local message = [[
There is no controller mapping for the %s.
To play with this controller, please create a mapping.

1. Run GeneralArcade Gamepad Tool. Follow the instructions to create a mapping string.
2. Copy and paste the mapping string to a new line in the gamecontrollerdb.txt file.
]]

	local buttons = {
		"Open Gamepad Tool website and gamecontrollerdb.txt file",
		"Continue without using this controller"
	}
	message = message:format(joystick:getName())
	local pressed = love.window.showMessageBox("Controller needs mapping", message, buttons)
	if pressed == 1 then
		love.system.openURL("https://www.generalarcade.com/gamepadtool/")
		love.system.openURL("file://"..love.filesystem.getSaveDirectory().."/gamecontrollerdb.txt")
		love.window.minimize()
	end
end

function love.load()
	if steam then
		steam.initialized = steam.init()
	end

	local version, err = love.filesystem.read("version")
	levity.version = version and version:sub(version:find("%S+"))

	if love.filesystem.getInfo("levity/gamecontrollerdb.txt") then
		love.joystick.loadGamepadMappings("levity/gamecontrollerdb.txt")
	end
	if love.filesystem.getInfo("gamecontrollerdb.txt") then
		love.joystick.loadGamepadMappings("gamecontrollerdb.txt")
	end
	love.joystick.saveGamepadMappings("gamecontrollerdb.txt")

--DEBUG---for i, joystick in ipairs(love.joystick.getJoysticks()) do
	--	print(i, joystick:getName())
	--	if joystick:isGamepad() then
	--		print(love.joystick.getGamepadMappingString(joystick:getGUID()))
	--	end
	--end

	local prefs = levity.prefs
	if levity.initDefaultPrefs then
		levity.initDefaultPrefs(prefs.defaultprefs)
	end
	prefs.init()

	local lapp = require "pl.lapp"
	lapp.slack = true

	assert(levity.nextmapfile, [[
	First map not set.
	In main.lua call levity:setNextMap to set the first map.
	]])

	local options = Usage.Desc..Usage.Console..Usage.Version..Usage.Fused
	if not love.filesystem.isFused() then
		options = options .. Usage.Game
	end
	options = options .. Usage.Debug..Usage.Prefs
	options = options .. string.format(Usage.Map, levity.nextmapfile)

	local args = lapp (options)

	if args.profile then
		profile = require "levity.profile.profile"
		profile.start()
	end

	if args.debug then
		require("mobdebug").start()
		require("mobdebug").off()
	end

	if args.map then
		-- When fused we expect <map>,
		-- but if fused mode is fake we get <game> and <map>
		-- Also handles invalid <map> by falling back to default
		if not love.filesystem.getInfo(args.map, "file") then
			args.map = args[1] or levity.nextmapfile
		end

		levity:setNextMap(args.map, levity.nextmapdata)
	end

	if args.rotation ~= -1 then
		prefs.rotation = args.rotation
	end
	prefs.exhibit = args.exhibit
	prefs.drawbodies = args.drawbodies
	prefs.drawstats = args.drawstats
	prefs.exclusive = args.exclusive
	if args.windowed then
		prefs.set("fullscreen", false)
	end
	prefs.applyAll()
	love.window.setTitle(love.filesystem.getIdentity())

	local conf = { modules = {} }
	love.conf(conf)
	if conf.icon then
		local imagedata = love.image.newImageData(conf.icon)
		if imagedata then
			love.window.setIcon(imagedata)
		end
	end

	local systemfont = levity.systemfont
	local systemfontsize = levity.systemfontsize
	systemfont = systemfont
			and love.filesystem.getInfo(systemfont)
			and love.graphics.newFont(systemfont, systemfontsize)
	systemfontsize = systemfontsize or love.graphics.getHeight()/64
	systemfont = systemfont or love.graphics.newFont(systemfontsize)
	systemfont:setFilter("nearest", "nearest")
	levity.systemfont = systemfont

	if args.buildmegatilesets then
		for file in love.filesystem.lines(args.buildmegatilesets) do
			if file:find(".lua$") then
				local data = love.filesystem.load(file)()
				if data.tilesets then
					Map(file, true)
				end
			end
		end
	end

	love.physics.setMeter(64)

	levity:loadNextMap()
end

function love.keypressed(key, u)
	levity.lastinputdevice = "keyboard"
	Scripts_broadcast(levity_scripts, "keypressed", key, u)
	Scripts_broadcast(levity_scripts, "keypressed_"..key, u)
end

function love.keyreleased(key, u)
	levity.lastinputdevice = "keyboard"
	Scripts_broadcast(levity_scripts, "keyreleased", key, u)
	Scripts_broadcast(levity_scripts, "keyreleased_"..key, u)
end

function love.touchpressed(touch, x, y, dx, dy, pressure)
	levity.lastinputdevice = touch
	Scripts_broadcast(levity_scripts, "touchpressed", touch, x, y)
end

function love.touchmoved(touch, x, y, dx, dy, pressure)
	levity.lastinputdevice = touch
	Scripts_broadcast(levity_scripts, "touchmoved", touch, x, y, dx, dy)
end

function love.touchreleased(touch, x, y, dx, dy, pressure)
	levity.lastinputdevice = touch
	Scripts_broadcast(levity_scripts, "touchreleased", touch, x, y, dx, dy)
end

-- 030000005e0400008e02000000000000,Controller (XBOX 360 For Windows),a:b0,b:b1,back:b6,dpdown:h0.4,dpleft:h0.8,dpright:h0.2,dpup:h0.1,leftshoulder:b4,leftstick:b8,lefttrigger:+a2,leftx:a0,lefty:a1,rightshoulder:b5,rightstick:b9,righttrigger:-a2,rightx:a3,righty:a4,start:b7,x:b2,y:b3,platform:Windows,
local DefaultDpadHat = {
	l = "dpleft",
	r = "dpright",
	u = "dpup",
	d = "dpdown"
}
local DefaultDpadButtons = {
	[13] = "dpleft",
	[14] = "dpright",
	[11] = "dpup",
	[12] = "dpdown"
}
local DefaultGamepadAxes = {
	"leftx", "lefty"
}
local DefaultGamepadButtons = {
	"a", "b", "x", "y", "leftshoulder", "rightshoulder", "back", "start",
	"leftstick", "rightstick", "guide", "dpup", "dpdown", "dpleft", "dpright"
}
function levity:setDefaultGamepadMappings(joystick)
	local guid = joystick:getGUID()
	if love.joystick.getGamepadMappingString(guid) then
		return
	end

	for i = 1, #DefaultGamepadButtons do
		love.joystick.setGamepadMapping(guid, DefaultGamepadButtons[i], "button", i)
	end
	if joystick:getHatCount() > 0 then
		for dir, button in pairs(DefaultGamepadHat) do
			love.joystick.setGamepadMapping(guid, button, "hat", 0, dir)
		end
	end
	for i = 1, #DefaultGamepadAxes do
		love.joystick.setGamepadMapping(guid, DefaultGamepadAxes[i], "axis", i)
	end
	return love.joystick.getGamepadMappingString(guid)
end

function love.joystickadded(joystick)
	levity.lastinputdevice = joystick:getGUID()
	Scripts_broadcast(levity_scripts, "joystickadded", joystick)
end

function love.joystickremoved(joystick)
	Scripts_broadcast(levity_scripts, "joystickremoved", joystick)
end

function love.joystickaxis(joystick, axis, value)
	if joystick:isGamepad() then
		return
	end
	levity.lastinputdevice = joystick:getGUID()
	Scripts_broadcast(levity_scripts, "joystickaxis", joystick, axis, value)
end

function love.joystickhat(joystick, hat, value)
	if joystick:isGamepad() then
		return
	end
	levity.lastinputdevice = joystick:getGUID()
	Scripts_broadcast(levity_scripts, "joystickhat", joystick, hat, value)
end

function love.joystickpressed(joystick, button)
	if joystick:isGamepad() then
		return
	end
	levity.lastinputdevice = joystick:getGUID()
	Scripts_broadcast(levity_scripts, "joystickpressed", joystick, button)
end

function love.joystickreleased(joystick, button)
	if joystick:isGamepad() then
		return
	end
	levity.lastinputdevice = joystick:getGUID()
	Scripts_broadcast(levity_scripts, "joystickreleased", joystick, button)
end

function love.gamepadaxis(joystick, axis, value)
	levity.lastinputdevice = joystick:getGUID()
	Scripts_broadcast(levity_scripts, "gamepadaxis", joystick, axis, value)
end

function love.gamepadpressed(joystick, button)
	levity.lastinputdevice = joystick:getGUID()
	Scripts_broadcast(levity_scripts, "gamepadpressed", joystick, button)
end

function love.gamepadreleased(joystick, button)
	levity.lastinputdevice = joystick:getGUID()
	Scripts_broadcast(levity_scripts, "gamepadreleased", joystick, button)
end

function love.mousepressed(x, y, button, istouch)
	if istouch then
		return
	end
	levity.lastinputdevice = "mouse"
	Scripts_broadcast(levity_scripts, "mousepressed", x, y, button, istouch)
end

function love.mousemoved(x, y, dx, dy, istouch)
	if istouch then
		return
	end
	levity.lastinputdevice = "mouse"
	Scripts_broadcast(levity_scripts, "mousemoved", x, y, dx, dy)
end

function love.mousereleased(x, y, button, istouch)
	if istouch then
		return
	end
	levity.lastinputdevice = "mouse"
	Scripts_broadcast(levity_scripts, "mousereleased", x, y, button, istouch)
end

function love.wheelmoved(x, y)
	levity.lastinputdevice = "mouse"
	Scripts_broadcast(levity_scripts, "wheelmoved", x, y)
end

function love.focus(focus)
	Scripts_broadcast(levity_scripts, "windowfocused", focus)
end

function love.resize(w, h)
	levity.resizeevent = {w, h}
end

function love.update(dt)
	if levity.resizeevent then
		local w = levity.resizeevent[1]
		local h = levity.resizeevent[2]
		applyResize(w, h)
		levity.resizeevent = nil
	end
	local movetimer = levity.movetimer
	local numfixedupdates = 0
	while movetimer <= 0 do
		local map = levity.map
		local bank = levity.bank
		local world = levity.world
		local movedt = levity.movedt
		local timescale = levity.timescale
		local discardedobjects = levity.discardedobjects

		Scripts_clearLogs(levity_scripts)

		local movedt = movedt * timescale
		if map.paused then
		else
			Scripts_broadcast(levity_scripts, "beginMove", movedt)
			world:update(movedt)
			Scripts_broadcast(levity_scripts, "endMove", movedt)
			map:update(movedt, levity_scripts)
		end

		Scripts_printLogs(levity_scripts)

		levity_cleanupObjects(levity, discardedobjects)

		if map.paused then
			Bank_update(bank, 0)
		else
			Bank_update(bank, movedt)
		end

		movetimer = movetimer + movedt
		numfixedupdates = numfixedupdates + 1
	end

	collectgarbage("step", 1)

	if levity.prefs.drawstats then
		levity.stats:update(dt)
	end

	dt = math.min(dt, levity.maxdt)

	movetimer = movetimer - dt
	levity.movetimer = movetimer

	if numfixedupdates > 0 then
		love.graphics.origin()
		love.graphics.clear(love.graphics.getBackgroundColor())
		love.draw()
		love.graphics.present()

		if steam and steam.initialized then
			steam.runCallbacks()
		end
	end

	if levity.nextmapfile then
		Scripts_broadcast(levity_scripts, "nextMap",
			levity.nextmapfile, levity.nextmapdata)
		levity:loadNextMap()
	end
end

function love.draw()
	if levity.drawBackground then
		levity.drawBackground()
	else
		love_graphics_clear(0, 0, 0)
	end

	levity.map:draw(levity.camera, levity_scripts,
		levity.prefs.drawbodies and levity.world)

	love_graphics_setFont(levity.systemfont)

	local rotation = math.rad(levity.prefs.rotation)
	local gw = love.graphics.getWidth()
	local gh = love.graphics.getHeight()
	local hgw = gw/2
	local hgh = gh/2
	local abscosr = math.abs(math.cos(rotation))
	local diff = abscosr*(hgw-hgh)
	local textwidth = hgh + diff
	local y = -hgw + diff

	love.graphics.push()
	love.graphics.translate(hgw, hgh)
	love.graphics.rotate(rotation)

	if levity.prefs.drawstats then
		levity.stats:draw(0, -hgw + diff, textwidth)
	end

	if levity.version and levity.showversion then
		local font = love_graphics_getFont()
		local y = hgw - diff - font:getHeight()
		love_graphics_printf("v"..levity.version, 0, y, textwidth, "right")
	end

	love.graphics.pop()
end

function love.quit()
	if profile then
		local filename = os.date("profile_%Y-%m-%d_%H-%M-%S")..".txt"
		love.filesystem.write(filename, profile.report())
	end
	if steam and steam.initialized then
		steam.shutdown()
	end
end

function love.run()
	if love.load then love.load(love.arg.parseGameArguments(arg), arg) end

	-- We don't want the first frame's dt to include time taken by love.load.
	if love.timer then love.timer.step() end

	local dt = 0

	-- Main loop time.
	return function()
		-- Process events.
		if love.event then
			love.event.pump()
			for name, a,b,c,d,e,f in love.event.poll() do
				if name == "quit" then
					if not love.quit or not love.quit() then
						return a or 0
					end
				end
				love.handlers[name](a,b,c,d,e,f)
			end
		end

		-- Update dt, as we'll be passing it to update
		if love.timer then dt = love.timer.step() end

		-- Call update and draw
		if love.update then love.update(dt) end -- will pass 0 if love.timer is disabled

		if love.timer then love.timer.sleep(0.001) end
	end
end

return levity
