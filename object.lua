local levity

local pairs = pairs
local ipairs = ipairs
local tonumber = tonumber

local math_abs = math.abs
local math_min = math.min
local math_max = math.max
local math_cos = math.cos
local math_sin = math.sin
local math_huge = math.huge

local love_graphics_draw = love.graphics.draw
local love_graphics_setFont = love.graphics.setFont
local love_graphics_printf = love.graphics.printf
local love_physics_newBody = love.physics.newBody
local love_physics_newFixture = love.physics.newFixture

local maputil = require "levity.maputil"
local maputil_setObjectDefaultProperties = maputil.setObjectDefaultProperties
local maputil_getAnimationUpdate = maputil.getAnimationUpdate

local scripting = require "levity.scripting"
local Scripts_send = scripting.newMachine.send
local Scripts_newScript = scripting.newMachine.newScript

local Tiles = require "levity.tiles"

--- @table DynamicObject
-- @field body
-- @field layer
-- @field tile Can be different from gid while animating
-- @field tileoffsetx Override tile offset
-- @field tileoffsety Override tile offset
-- @field shearx
-- @field sheary
-- @field animation
-- @field anitime in milliseconds
-- @field anitimescale
-- @field aniframe
-- @see Object

local Object = {}
Object.__index = Object
function Object.__lt(object1, object2)
	local dy = object1.y - object2.y
	return dy < 0 or (dy == 0 and object1.id < object2.id)
end

local function addFixture(body, shape, object)
	local fixture = love_physics_newFixture(body, shape)
	fixture:setSensor(object.properties.sensor == true)
	fixture:setRestitution(object.properties.restitution or 0)
	fixture:setUserData({
		id = object.id,
		object = object,
		properties = object.properties
	})

	local collisionrules = levity.collisionrules
	local category = object.properties.category
	if category and collisionrules then
		if type(category) == "string" then
			category = levity.collisionrules["Category_"..category]
		end
		fixture:setCategory(category)
	end

	return fixture
end

local function addTileFixture(object, shapeobj, tileset, map)
	local shape
	if shapeobj.shape == "rectangle" then
		local x, y = map:getTileShapePosition(object.gid, shapeobj, true)
		shape = love.physics.newRectangleShape(x, y,
			shapeobj.width, shapeobj.height)
	elseif shapeobj.shape == "ellipse" then
		local x, y = map:getTileShapePosition(object.gid, shapeobj, true)
		shape = love.physics.newCircleShape(x, y,
			(shapeobj.width + shapeobj.height) * .25)
	elseif shapeobj.shape == "polyline" or shapeobj.shape == "polygon" then
		local x, y = map:getTileShapePosition(object.gid, shapeobj, false)
		local points = {}
		local poly = shapeobj.polygon or shapeobj.polyline
		for i = 1, #poly do
			local point = poly[i]
			points[#points+1] = point.x + x
			points[#points+1] = point.y + y
		end
		if shapeobj.polygon then
			shape = love.physics.newPolygonShape(points)
		else
			shape = love.physics.newChainShape(points)
		end
	end

	local fixture = addFixture(object.body, shape, shapeobj)

	if shapeobj.name ~= "" then
		local bodyud = object.body:getUserData()
		if not bodyud.fixtures then
			bodyud.fixtures = {}
		end
		bodyud.fixtures[shapeobj.name] = fixture
	end
end

function Object.isDrawable(object)
	return object.gid or object.text or object.properties.drawable
end
Object_isDrawable = Object.isDrawable

function Object.setNextGid(object, gid, animated, bodytype, applyfixtures)
	object.nextgid = gid
	object.nextgidanimated = animated
	object.nextgidbodytype = bodytype
	object.nextgidapplyfixtures = applyfixtures
end

function Object.setGid(object, gid, animated, bodytype, applyfixtures)
	levity = levity or require "levity"
	local world = levity.world
	if world:isLocked() then
		Object.setNextGid(object, gid, animated, bodytype, applyfixtures)
		return
	end
	local map = levity.map
	local newtile = map.tiles[gid]
	local newtileset = map.tilesets[newtile.tileset]
	if applyfixtures == nil then
		applyfixtures = object.body == nil or
			(newtileset.properties.commoncollision == nil and
			gid ~= object.gid) or
			not object.tile or
			newtile.tileset ~= object.tile.tileset
	end

	object.gid = gid
	object.tile = newtile

	local properties = object.properties

	bodytype = bodytype or properties.static == true and "static"

	local body = object.body
	if body then
		if applyfixtures then
			for _, fixture in pairs(body:getFixtures()) do
				fixture:destroy()
			end
		end
		bodytype = bodytype or properties.static == false and "dynamic"
		if bodytype and bodytype ~= body:getType() then
			body:setType(bodytype)
		end

		body:setActive(true)
	else
		body = love.physics.newBody(world, object.x, object.y,
						bodytype or "dynamic")
		body:setAngle(object.rotation)
		object.body = body
	end

	local userdata = body:getUserData()
	if not userdata then
		userdata = {}
		body:setUserData(userdata)
	end
	userdata.id = object.id
	userdata.object = object
	userdata.properties = object.properties
	if applyfixtures then
		userdata.fixtures = nil
	end

	applyfixtures = applyfixtures
		and properties.collidable ~= false
		and object.layer.properties.collidable ~= false

	local objectgroup = object.tile.objectGroup
	if applyfixtures and objectgroup then
		local objects = objectgroup.objects
		for i = 1, #objects do
			local shapeobj = objects[i]
			if shapeobj.properties.collidable == true then
				addTileFixture(object, shapeobj, newtileset, map)
			end
		end
	end

	if animated == nil then
		animated = object.properties.animated
	end
	if animated == nil then
		animated = true
	end
	local animation = object.tile.animation
	if animated and animation then
		object.animation = animation
		if animated == "keeptimer" and object.aniframe
		and object.aniframe <= #animation then
			local tileid = animation[object.aniframe].tileid
			object.tile = map.tiles[newtileset.firstgid + tileid]
		else
			object.anitime = 0
			object.anitimescale = 1
			object.aniframe = 1
		end
	else
		object.animation = nil
		object.anitime = nil
		object.anitimescale = nil
		object.aniframe = nil
	end
end
local Object_setGid = Object.setGid

function Object.setTileId(object, tileid, animated, bodytype, applyfixtures)
	local tileset = object.tile and object.tile.tileset
	if not tileset then
		return
	end
	local gid = levity.map:getTileGid(tileset, tileid)
	if gid then
		gid = Tiles.setGidFlip(gid, Tiles.getGidFlip(object.gid))
		Object_setGid(object, gid, animated, bodytype, applyfixtures)
	end
end
local Object_setTileId = Object.setTileId

local function removeObject(objects, object)
	for i, o in pairs(objects) do
		if o == object then
			table.remove(objects, i)
			return
		end
	end
end

function Object.setLayer(object, layer)
	local oldlayer = object.layer
	if oldlayer == layer then
		return
	end

	if oldlayer then
		removeObject(oldlayer.objects, object)
		if oldlayer.spriteobjects then
			if Object_isDrawable(object) then
				removeObject(oldlayer.spriteobjects, object)
			end
		end
	end

	if layer then
		local objects = layer.objects
		local spriteobjects = layer.spriteobjects
		objects[#objects+1] = object
		if Object_isDrawable(object) then
			spriteobjects[#spriteobjects+1] = object
		end
	end
	object.layer = layer
end
local Object_setLayer = Object.setLayer

function Object.init(object, layer, map)
	levity = levity or require "levity"
	object = setmetatable(object, Object)

	if object.visible == nil then
		object.visible = true
	end
	object.rotation = object.rotation or 0
	local properties = object.properties or {}
	object.properties = properties

	if map.objecttypes then
		maputil_setObjectDefaultProperties(object, map.objecttypes)
	end

	local id = object.id

	Object_setLayer(object, layer)

	if object.gid then
		Object_setGid(object, object.gid)
	else
		local bodytype = properties.static and "static" or "dynamic"
		local body = object.body or love_physics_newBody(levity.world,
							object.x, object.y,
							bodytype)
		object.body = body

		body:setAngle(object.rotation)
		local userdata = body:getUserData() or {}
		userdata.id = id
		userdata.object = object
		userdata.properties = properties
		body:setUserData(userdata)

		local collidable =
			properties.collidable == true or
			layer.properties.collidable == true

		if collidable then
			local shape = nil
			if object.shape == "rectangle" then
				shape = love.physics.newRectangleShape(
					object.width * .5, object.height * .5,
					object.width, object.height)
			elseif object.shape == "ellipse" then
				local halfw, halfh = object.width*.5, object.height*.5
				shape = love.physics.newCircleShape(halfw, halfh,
					(object.width + object.height) * .25)
			elseif object.shape == "polyline" or object.shape == "polygon" then
				local points = {}
				local poly = object.polygon or object.polyline
				for _, point in ipairs(poly) do
					-- sti converts them to world points
					table.insert(points, point.x - object.x)
					table.insert(points, point.y - object.y)
				end
				shape = love.physics.newChainShape(object.polygon ~= nil, points)
			end

			if shape then
				addFixture(body, shape, object)
			end
		end
	end

	map.objects[id] = object
	Scripts_newScript(levity.scripts, id, properties.script, object)
end

function Object.updateAnimation(object, dt, map, scripts)
	local aniframe, looped
	aniframe, object.anitime, looped =
		maputil_getAnimationUpdate(object.animation,
					object.aniframe, object.anitime,
					dt * (object.anitimescale or 1))

	local nextanimtileid = object.properties.nextanimtileid

	if looped > 0 and nextanimtileid then
		Object_setTileId(nextanimtileid)
	elseif aniframe ~= object.aniframe then
		object.aniframe = aniframe
		local frame = object.animation[aniframe]
		local tileid = frame.tileid
		local tileset = object.tile.tileset
		local gid = levity.map:getTileGid(tileset, tileid)
		gid = Tiles.setGidFlip(gid, Tiles.getGidFlip(object.gid))
		object.tile = levity.map.tiles[gid]
	end

	if looped > 0 then
		if object.properties.animenddiscard then
			levity = levity or require "levity"
			levity:discardObject(object.id)
		end
		Scripts_send(scripts, object.id, "loopedAnimation")
	end
end

function Object.isOnCamera(object, camera)
	local camw = camera.w
	local camh = camera.h
	local camcx = camera.x + camw*.5
	local camcy = camera.y + camh*.5
	local layer = object.layer

	return not (math_abs(layer.offsetx + object.x - camcx) > camw
			or math_abs(layer.offsety + object.y - camcy) > camh)
end

function Object.getBoundingBox(object)
	local x1, y1, x2, y2
	local body = object.body

	if body then
		x1, y1, x2, y2 = math_huge, math_huge, -math_huge, -math_huge
		for _, fixture in pairs(body:getFixtures()) do
			local l, t, r, b = fixture:getBoundingBox()
			x1 = math_min(x1, l)
			y1 = math_min(y1, t)
			x2 = math_max(x2, r)
			y2 = math_max(y2, b)
		end
	else
		x1 = object.x
		y1 = object.y
		x2 = x1 + object.width
		y2 = y1 + object.height
		local tile = object.tile
		if tile then
			local offset = tile.offset
			x1 = x1 + offset.x
			y1 = y1 + offset.y - tile.height
		end
	end
	return x1, y1, x2, y2
end

local function Object_getRGBA(object)
	local r,g,b,a = 1, 1, 1, 1
	local color = object.color
	if color then
		if type(color) == "string" then
			a,r,g,b = levity.parseARGB(color)
		elseif type(color) == "table" then
			r, g, b, a = color[1], color[2],
				color[3], color[4] or a
		end
	end
	return r, g, b, a
end

function Object.updateBatchSprite(object)
	local tile = object.tile
	if not tile then
		return
	end

	local image = tile.image
	local batches = object.layer.batches
	local batch = batches and batches[image]
	local spriteidx = object.spriteidx
	local visible = object.visible
	if not visible then
		if spriteidx then
			batch:set(spriteidx, 0, 0, 0, 0, 0)
		end
		return
	end

	if not batch then
		batch = love.graphics.newSpriteBatch(image)
		batch:setColor(1, 1, 1, 1)
		batches[image] = batch
	end
	local x, y = object.x, object.y
	local ox = object.properties.tileoffsetx or tile.offset.x
	local oy = object.properties.tileoffsety or tile.offset.y
	ox = -ox
	oy = -oy + tile.height
	local sx = tile.sx * (object.scalex or 1)
	local sy = tile.sy * (object.scaley or 1)
	local rotation = tile.r + object.rotation
	if not spriteidx then
		levity = levity or require "levity"
		local freespriteidxs = levity.map.freeBatchSprites[batch]
		if freespriteidxs and #freespriteidxs > 0 then
			spriteidx = freespriteidxs[#freespriteidxs]
			freespriteidxs[#freespriteidxs] = nil
			object.spriteidx = spriteidx
		end
	end
	local r, g, b, a = Object_getRGBA(object)
	batch:setColor(r, g, b, a)
	if spriteidx then
		batch:set(spriteidx, tile.quad, x, y, rotation, sx, sy, ox, oy)
	else
		spriteidx = batch:add(tile.quad, x, y, rotation, sx, sy, ox, oy)
		object.spriteidx = spriteidx
	end
end

function Object.freeBatchSprite(object)
	local spriteidx = object.spriteidx
	if not spriteidx then
		return
	end
	local tile = object.tile
	local image = tile.image
	local batches = object.layer.batches
	local batch = batches and batches[image]
	if not batch then
		return
	end
	batch:set(spriteidx, 0, 0, 0, 0, 0)
	levity = levity or require "levity"
	local freespriteidxs = levity.map.freeBatchSprites[batch] or {}
	levity.map.freeBatchSprites[batch] = freespriteidxs
	freespriteidxs[#freespriteidxs + 1] = spriteidx
	object.spriteidx = nil
end

function Object.draw(object)
	levity = levity or require "levity"
	local properties = object.properties
	local left = object.x
	local top = object.y
	local width = object.width
	local height = object.height
	local right = left + (width or 0)
	local bottom = top + (height or 0)

	local color = object.color
	local r0,g0,b0,a0 = 1, 1, 1, 1
	if color then
		r0,g0,b0,a0 = love.graphics.getColor()
		local r,g,b,a = Object_getRGBA(object)
		love.graphics.setColor(r, g, b, a)
	end

	local tile = object.tile
	if tile then
		local tw = tile.width
		local th = tile.height
		local x = left
		local y = top
		local ox = object.properties.tileoffsetx or tile.offset.x
		local oy = object.properties.tileoffsety or tile.offset.y
		ox = -ox
		oy = -oy + th
		local sx = tile.sx * (object.scalex or 1)
		local sy = tile.sy * (object.scaley or 1)
		local r = tile.r + object.rotation
		left = x - ox
		top = y - oy
		right = left + tw
		bottom = top + th

		local shearx = object.shearx or 0
		local sheary = object.sheary or 0
		love_graphics_draw(tile.image, tile.quad, x, y, r, sx, sy,
					ox, oy, shearx, sheary)
	elseif object.body then
		local body = object.body
		for j, fixture in ipairs(body:getFixtures()) do
			local l, t, r, b = fixture:getBoundingBox()
			left = math_min(left, l)
			top = math_min(top, t)
			right = math_max(right, r)
			bottom = math_max(bottom, b)

			local shape = fixture:getShape()
			if shape:getType() == "circle" then
				local x, y = body:getWorldPoint(
					shape:getPoint())
				love.graphics.circle("line", x, y,
					shape:getRadius())
			elseif shape:getType() == "polygon" then
				love.graphics.polygon("line",
				body:getWorldPoints(shape:getPoints()))
			elseif shape:getType() == "chain" then
				love.graphics.line(
				body:getWorldPoints(shape:getPoints()))
			end
		end
	end

	local text = object.text
	if text then
		local font = object.fontfamily
		local fontsize = object.pixelsize or 16
		local bold = object.bold
		local italic = object.italic
		local fonttype = object.properties.fonttype or "fnt"
		text = levity.prefs.string_gsub(text)
		font = font and levity.fonts:use(font, fontsize, bold, italic, fonttype)
		if not font then
			local systemfont = levity.systemfont
			love_graphics_setFont(systemfont)
			font = systemfont
		end

		local textalign = object.halign or "left"

		local offsety = 0
		local valign = object.valign
		if valign then
			local _, textlines = font:getWrap(text, width)
			offsety = height - font:getHeight()*#textlines
			if valign == "center" then
				offsety = offsety / 2
			end
		end
		love_graphics_printf(text, left, top + offsety, right - left,
					textalign)--, object.rotation)
	end

	if color then
		love.graphics.setColor(r0, g0, b0, a0)
	end
end

return Object
