--- @module audio

local MusicEmu = require "levity.MusicEmu"

local Bank = class()
function Bank:_init()
	self.sounds = {}
	self.music = nil
	self.musicfadespeed = 0
end

local EmuFileTypes = {
	[".vgm"] = true,
	[".vgz"] = true
}

local FileTypes = {
	[".ogg"] = true,
	[".wav"] = true
}

function Bank.isAudioFile(filename)
	return type(filename) == "string" and FileTypes[filename:sub(-4, -1)]
		and not filename:match("%%d")
end

function Bank.isMusicEmuFile(filename)
	return type(filename) == "string" and EmuFileTypes[filename:sub(-4, -1)]
		and not filename:match("%%d")
end

--- Load list of audio files
-- @param sounds Table or comma-separated audio file list
-- @param type "static" for sfx or "stream" for music/ambience (default "static")
function Bank:load(soundfiles, typ)
	typ = typ or "static"
	local function load(soundfile)
		local sound = self.sounds[soundfile]
		if not sound or sound:getType() ~= typ then
			if love.filesystem.getInfo(soundfile) then
				if MusicEmu and Bank.isMusicEmuFile(soundfile) then
					sound = MusicEmu(soundfile)
				else
					sound = love.audio.newSource(soundfile, typ)
				end

				self.sounds[soundfile] = sound
			else
				print("WARNING: Missing sound file "..soundfile)
			end
		end
	end

	if type(soundfiles) == "table" then
		for _, soundfile in pairs(soundfiles) do
			load(soundfile)
		end
	elseif type(soundfiles) == "string" then
		for soundfile in (soundfiles..','):gmatch("%s*(.-)%s*,%s*") do
			load(soundfile)
		end
	end

end

--- Play an audio file
-- @param soundfile
-- @return Sound source now playing the audio
function Bank:play(soundfile, newsource)
	local levity = require "levity"
	local sound = self.sounds[soundfile]
	local source = nil
	if sound then
		local typ = sound:getType()
		local volume
		if typ == "static" then
			volume = levity.prefs.soundvolume
			source = newsource and sound:clone() or sound
		else
			volume = levity.prefs.musicvolume
			source = sound
		end
		if source:isPlaying() then
			source:stop()
		end
		source:setVolume(volume)
		source:play()
	end
	return source
end

--- Randomly select and play from a numbered set of sounds
-- e.g. playRandom("sound%d.wav") plays one of sound1.wav, sound2.wav, sound3.wav
-- @param soundfilepattern containing "%d" for the index
-- @return Sound source now playing the audio
function Bank:playRandom(soundfilepattern)
	local n = 0
	while self.sounds[soundfilepattern:format(n+1)] do
		n = n + 1
	end
	if n > 0 then
		return self:play(soundfilepattern:format(love.math.random(n)))
	end
end

function Bank:setMusicVolume(volume)
	if self.music then
		self.music:setVolume(volume)
	end
end

function Bank:update(dt)
	if self.music then
		if self.music.update then
			self.music:update()
		end

		if self.musicfadespeed > 0 then
			local volume = self.music:getVolume() - self.musicfadespeed*dt
			if volume <= 0 then
				self.music:stop()
				self.music = nil
				self.musicfadespeed = 0
			else
				self.music:setVolume(volume)
			end
		end
	end
end

function Bank:playMusic(file)
	if self.music then
		self.music:stop()
	end
	self.music = self:play(file)
	self.musicfadespeed = 0
	return self.music
end

function Bank:fadeMusic(time)
	if self.music then
		time = time or 3
		self.musicfadespeed = self.music:getVolume()/time
	end
	return self.music
end

local audio = {
	newBank = Bank
}

return audio
