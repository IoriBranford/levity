local ffi = require "ffi"

local ok, lib = pcall(ffi.load, "vgm-player")
if not ok then
	print(lib)
	return false
end

ffi.cdef[[
typedef struct VGMPlayer VGMPlayer;

VGMPlayer* vgmNewPlayer();
void vgmDeletePlayer(VGMPlayer *vgmPlayer);

//uint32_t GetPlayerType(void) const;
//const char* GetPlayerName(void) const;
uint8_t vgmLoadFile(VGMPlayer *vgmPlayer, const char* fileName);
uint8_t vgmLoadData(VGMPlayer *vgmPlayer, size_t fileSize, uint8_t* fileData);
uint8_t vgmUnload(VGMPlayer *vgmPlayer);

//const VGM_HEADER* GetFileHeader(void) const
const char* vgmGetSongTitle(VGMPlayer *vgmPlayer);
////uint32_t GetSampleRate(void) const
uint8_t vgmSetSampleRate(VGMPlayer *vgmPlayer, uint32_t sampleRate);
////uint8_t vgmSetPlaybackSpeed(VGMPlayer *vgmPlayer, double speed);
////void vgmSetCallback(VGMPlayer *vgmPlayer, PLAYER_EVENT_CB cbFunc, void* cbParam);

uint32_t vgmTick2Sample(const VGMPlayer *vgmPlayer, uint32_t ticks);
uint32_t vgmSample2Tick(const VGMPlayer *vgmPlayer, uint32_t samples);
double vgmTick2Second(const VGMPlayer *vgmPlayer, uint32_t ticks);
////double vgmSample2Second(uint32_t samples);

uint8_t vgmGetState(const VGMPlayer *vgmPlayer);
uint32_t vgmGetCurFileOfs(const VGMPlayer *vgmPlayer);
uint32_t vgmGetCurTick(const VGMPlayer *vgmPlayer);
uint32_t vgmGetCurSample(const VGMPlayer *vgmPlayer);
uint32_t vgmGetTotalTicks(const VGMPlayer *vgmPlayer);	// get time for playing once in ticks
uint32_t vgmGetLoopTicks(const VGMPlayer *vgmPlayer);	// get time for one loop in ticks
////uint32_t vgmGetTotalPlayTicks(const VGMPlayer *vgmPlayer, uint32_t numLoops);	// get time for playing + looping (without fading)
uint32_t vgmGetCurrentLoop(const VGMPlayer *vgmPlayer);

uint8_t vgmStart(VGMPlayer *vgmPlayer);
uint8_t vgmStop(VGMPlayer *vgmPlayer);
uint8_t vgmReset(VGMPlayer *vgmPlayer);

uint32_t vgmPlay(VGMPlayer *vgmPlayer, uint32_t smplCnt, int16_t* data);
////uint8_t vgmSeek(...); // TODO
]]

ffi.metatype("VGMPlayer", {
	__gc = function(vgmplayer)
		lib.vgmStop(vgmplayer)
		lib.vgmUnloadFile(vgmplayer)
		lib.vgmDeletePlayer(vgmplayer)
	end
})

local VGMPlayer = {}

function VGMPlayer:getType()
	return "emu"
end

function VGMPlayer:start(track)
	lib.vgmStart(self.vgmplayer)
end

function VGMPlayer:update()
	--self.source:step()

	while self.source:getFreeBufferCount() > 0 do
		lib.vgmPlay(self.vgmplayer, self.buffersamples, self.sounddata:getFFIPointer())
		self.source:queue(self.sounddata)
		self.source:play()
	end
end

function VGMPlayer:unpause()
	self.source:play()
end

function VGMPlayer:pause()
	self.source:pause()
end

function VGMPlayer:setVolume(v)
	self.source:setVolume(v)
end

function VGMPlayer:fade()
	--TODO
end

function VGMPlayer:isPlaying()
	return lib.vgmIsPlaying(self.vgmplayer) ~= 0
end

local function newVGMPlayer(filename, buffersamples, rate)
	local data, error = love.filesystem.newFileData(filename)
	if not data then
		print(error)
		return nil
	end
	if data:getExtension() == "vgz" then
		local str = love.data.decompress(data, "gzip")
		data = love.filesystem.newFileData(str, "decompressedmusic")
	end

	local self = {}
	for k,v in pairs(VGMPlayer) do
		self[k] = v
	end
	self.buffersamples = buffersamples or 2048
	self.rate = rate or 48000

	local vgmplayer = lib.vgmNewPlayer()
	self.vgmplayer = vgmplayer
	lib.vgmLoadData(vgmplayer, data:getSize(), data:getFFIPointer())
	lib.vgmSetSampleRate(vgmplayer, self.rate)
	self.source = love.audio.newQueueableSource(self.rate, 16, 2, 2)
	self.sounddata = love.sound.newSoundData(self.buffersamples, self.rate)
	--ffi.fill(self.sounddata:getFFIPointer(), self.sounddata:getSize())
	self:setVolume(0.5)
	return self
end

return newVGMPlayer
