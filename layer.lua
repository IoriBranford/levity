local pairs = pairs
local table_sort = table.sort
local table_insert = table.insert

local love_graphics_draw = love.graphics.draw

local Object = require "levity.object"
local Object_init = Object.init
local Object_setGid = Object.setGid
local Object_updateAnimation = Object.updateAnimation
local Object_updateBatchSprite = Object.updateBatchSprite
local Object___lt = Object.__lt
local Object_isOnCamera = Object.isOnCamera
local Object_draw = Object.draw
local Object_isDrawable = Object.isDrawable

local scripting = require "levity.scripting"
local Scripts_send = scripting.newMachine.send

--- @table DynamicLayer
-- @field type "dynamiclayer"
-- @field newobjects
-- @field objects
-- @field spriteobjects
-- @see ObjectLayer

--- @table TileLayer.properties
-- @field scrollvelx
-- @field scrollvely
-- @field tilewrap
-- @field tilewrapx1
-- @field tilewrapy1
-- @field tilewrapx2
-- @field tilewrapy2
-- @see TileLayer

local Layer = {}
Layer.__index = Layer

function Layer.addObject(layer, object)
	layer.newobjects[#layer.newobjects + 1] = object
end

local LayerMaxNewObjects = 256
local LayerAddedTooManyObjectsMessage =
[[Tried to add too many (]]..LayerMaxNewObjects..[[) objects at a time to one
layer. Avoid recursive object creation in object init functions.]]

function Layer.updateObjects(layer, dt, map, scripts)
	local newobjects = layer.newobjects
	local i0 = 1
	local i1 = #newobjects

	while i0 <= i1 do
		for i = i0, i1 do
			Object_init(newobjects[i], layer, map)
		end
		for i = i0, i1 do
			Scripts_send(scripts, newobjects[i].id, "initQuery")
		end

		i0 = i1 + 1
		i1 = #newobjects

		assert(i1 <= LayerMaxNewObjects, LayerAddedTooManyObjectsMessage)
	end

	for i = #newobjects, 1, -1 do
		newobjects[i] = nil
	end

	for _, object in pairs(layer.spriteobjects) do
		local body = object.body
		if body then
			object.x = (body:getX())
			object.y = (body:getY())
			object.rotation = body:getAngle()
		end

		if object.animation then
			Object_updateAnimation(object, dt, map, scripts)
		end

		local nextgid = object.nextgid
		if nextgid then
			Object_setGid(object, nextgid,
				object.nextgidanimated,
				object.nextgidbodytype,
				object.nextgidapplyfixtures)

			object.nextgid = nil
			object.nextgidanimated = nil
			object.nextgidbodytype = nil
			object.nextgidapplyfixtures = nil
		end
	end

	if layer.batches then
		for _, object in pairs(layer.spriteobjects) do
			Object_updateBatchSprite(object)
		end
	end

	if layer.visible and layer.draworder == "topdown" then
		table_sort(layer.spriteobjects, Object___lt)
	end
end

function Layer.drawObjects(layer, camera, scripts)
	local spriteobjects = layer.spriteobjects
	for i = 1, #spriteobjects do
		local object = spriteobjects[i]
		if object.visible then--and Object_isOnCamera(object, camera) then
			local id = object.id
			if scripts then
				Scripts_send(scripts, id, "beginDraw")
			end
			Object_draw(object)
			if scripts then
				Scripts_send(scripts, id, "endDraw")
			end
		end
	end
end

function Layer.drawImage(layer)
	if layer.image then
		local tintcolor = layer.tintcolor
		if tintcolor then
			local r = (1+tintcolor[1])
			local g = (1+tintcolor[2])
			local b = (1+tintcolor[3])
			love.graphics.setColor(r/256, g/256, b/256, layer.opacity)
		end
		love_graphics_draw(layer.image)
		if tintcolor then
			love.graphics.setColor(1,1,1)
		end
	end
end

function Layer.drawBatches(layer, camera, scripts, map)
	if layer.properties.tilewrap then
		local wrapx1 = (layer.properties.tilewrapx1) * map.tilewidth
		local wrapy1 = (layer.properties.tilewrapy1) * map.tileheight
		local wrapx2 = (layer.properties.tilewrapx2) * map.tilewidth
		local wrapy2 = (layer.properties.tilewrapy2) * map.tileheight
		local wrapw = wrapx2 - wrapx1
		local wraph = wrapy2 - wrapy1

		local x1 = camera.x - wrapx1 - layer.offsetx
		local y1 = camera.y - wrapy1 - layer.offsety
		x1 = math.floor(x1 / wrapw) * wrapw
		y1 = math.floor(y1 / wraph) * wraph
		local x, y = x1, y1
		while y < camera.h do
			while x < camera.w do
				for _, batch in pairs(layer.batches) do
					love_graphics_draw(batch, x, y)
				end
				x = x + wrapw
			end
			x = x1
			y = y + wraph
		end
	else
		for _, batch in pairs(layer.batches) do
			love_graphics_draw(batch)
		end
	end
end

function Layer.toSpriteBatchObjectLayer(layer)
	layer.batches = {}
	layer.update = Layer.updateObjects
	layer.draw = Layer.drawBatches
end

function Layer.init(layer)
	--TODO: Why doesn't setting metatable work here?
	--setmetatable(layer, Layer)
	for fname, f in pairs(Layer) do
		if fname ~= "__call" then
			layer[fname] = f
		end
	end

	if layer.type == "imagelayer" then
		layer.draw = Layer.drawImage
	elseif layer.type == "tilelayer" or layer.type == "objectgroup" then
		layer.draw = Layer.drawBatches
	elseif layer.type == "dynamiclayer" then
		layer.update = Layer.updateObjects
		layer.draw = Layer.drawObjects

		layer.newobjects = {}
		-- why newobjects is necessary:
		-- http://www.lua.org/manual/5.1/manual.html#pdf-next
		-- "The behavior of next [and therefore pairs] is undefined if, during
		-- the traversal, you assign any value to a non-existent field in the
		-- table [i.e. a new object]."
		local spriteobjects = {}
		layer.spriteobjects = spriteobjects
		if layer.objects then
			if layer.draworder == "topdown" then
				table_sort(layer.objects, Object___lt)
			end
			for _, object in pairs(layer.objects) do
				if Object_isDrawable(object) then
					table_insert(spriteobjects, object)
				end
				object.layer = layer
			end
		end

		layer.objects = layer.objects or {}
		if layer.properties.batchsprites then
			layer:toSpriteBatchObjectLayer()
		end
	end

	layer.offsetx = layer.offsetx or 0
	layer.offsety = layer.offsety or 0
end

function Layer.__call(_, map, name, i)
	local layer = map:addCustomLayer(name, i)
	Layer.init(layer)
	return layer
end

return Layer
