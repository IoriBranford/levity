local Stats = class()
function Stats:_init()
	self:reset()
end

function Stats:reset()
	self.timer = 0
	self.rate = 1
end

function Stats:update(dt)
	self.timer = self.timer + dt
	while self.timer > self.rate do
		-- nothing yet
		self.timer = self.timer - self.rate
	end
end

function Stats:draw(x, y, w)
	local font = love.graphics.getFont()

	local fps = love.timer.getFPS()
	local mem = math.floor(collectgarbage('count'))
	local gfxstats = love.graphics.getStats()
	local fonth = font:getHeight()

	love.graphics.printf(fps.."fps", x, y, w, "right")
	y = y + fonth
	love.graphics.printf(mem.." kb", x, y, w, "right")
	y = y + fonth

	for stat, value in pairs(gfxstats) do
		love.graphics.printf(value..' '..stat, x, y, w, "right")
		y = y + fonth
	end
end

local stats = {
	newStats = Stats
}

return stats
