-- Can't use actual bitops - they return signed whereas GIDs are always unsigned
-- Source: http://bitop.luajit.org/semantics.html#range

local bit31   = 2147483648
local bit30   = 1073741824

local Tiles = {}

function Tiles.getGidFlip(gid)
	local flipX   = false
	local flipY   = false

	if gid >= bit31 then
		gid = gid - bit31
		flipX   = not flipX
	end

	if gid >= bit30 then
		gid = gid - bit30
		flipY   = not flipY
	end

	return flipX, flipY
end

function Tiles.getUnflippedGid(gid)
	if gid >= bit31 then
		gid = gid - bit31
	end

	if gid >= bit30 then
		gid = gid - bit30
	end

	return gid
end

function Tiles.setGidFlip(gid, flipx, flipy)
	local realgid = Tiles.getUnflippedGid(gid)
	if flipx == true then
		realgid = realgid + bit31
	end

	if flipy == true then
		realgid = realgid + bit30
	end

	return realgid
end

return Tiles
