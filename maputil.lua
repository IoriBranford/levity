local pl = require "levity.pl.lua.pl.import_into"()
local tonumber = tonumber
local pairs = pairs

local MapUtil = {}

local ObjectTypesTemplate = [[
<objecttypes>
{{<objecttype name="$_">
{{<property name="$_" type="$type" default="$value"/>}}
 </objecttype>}}
</objecttypes>
]]

--- XML file into object types table
-- @param filename
-- @return Table in the form { Type = { Property = Value, ... }, ... }
function MapUtil.loadObjectTypes(filename)
	local status, err
	local file = love.filesystem.newFile(filename)
	if not file then
		return false
	end

	status, err = file:open("r")
	if not status then
		print(err)
		return false
	end

	local text, _ = file:read()
	file:close()

	local doc = pl.xml.parse(text, false)
	local objecttypes = doc:match(ObjectTypesTemplate)
	return objecttypes
end

function MapUtil.loadObjectTypesFile(filename)
	local objecttypes = MapUtil.loadObjectTypes(filename)
	if objecttypes then
		MapUtil.setObjectTypesProperties(objecttypes)
	end
	return objecttypes
end

function MapUtil.setObjectTypesProperties(objecttypes)
	for _, properties in pairs(objecttypes) do
		for property, value in pairs(properties) do
			if value.value == "true" then
				properties[property] = true
			elseif value.value == "false" then
				properties[property] = false
			elseif value.type == "int" or value.type == "float" or value.type == "object" then
				properties[property] = tonumber(value.value)
			else
				properties[property] = value.value
			end
		end
	end
end

--- Set object's properties to fall back to default properties
-- Precedence:
-- 1. object
-- 2. object type
-- If no object type:
-- 3. tile
-- 4. tile type
--@param object
--@param objecttypes returned from loadObjectTypesFile
function MapUtil.setObjectDefaultProperties(object, objecttypes)
	if not object.properties then
		return
	end

	local mt = {}
	function mt.__index(_, key)
		local defaultproperties = objecttypes[object.type]
		if defaultproperties then
			local value = defaultproperties and defaultproperties[key]
			if value ~= nil then return value end
		else
			local tile = object.tile
			return tile and tile.properties[key]
		end
	end

	setmetatable(object.properties, mt)
end

--- Set multiple objects' default properties
--@param objects array
--@param objecttypes returned from loadObjectTypesFile
function MapUtil.setObjectsDefaultProperties(objects, objecttypes)
	for _, object in pairs(objects) do
		MapUtil.setObjectDefaultProperties(object, objecttypes)
	end
end

--- Set object type to fall back to another object type as its base
--@param object
--@param objecttypes returned from loadObjectTypesFile
function MapUtil.setObjectTypeBase(objecttype, objecttypes)
	local mt = {}
	function mt.__index(objecttype, property)
		local basetype = rawget(objecttype, "_basetype")
		basetype = basetype and objecttypes[basetype]
		return basetype and basetype[property]
	end

	setmetatable(objecttype, mt)
end

--- Set multiple object types' bases
--@param objecttypes returned from loadObjectTypesFile
function MapUtil.setObjectTypesBases(objecttypes)
	for _, objecttype in pairs(objecttypes) do
		MapUtil.setObjectTypeBase(objecttype, objecttypes)
	end
end

--- Get update frame and timer for animation
--@param animation
--@param frame
--@param time in ms
--@param dt The delta time
--@return new frame
--@return new timer in ms
--@return number of times looped
function MapUtil.getAnimationUpdate(animation, frame, time, dt)
	local nf = #animation
	local duration = animation[frame].duration
	local loops = 0
	time = time + dt * 1000
	while time > duration do
		time = time - duration
		frame = frame + 1
		if frame > nf then
			loops = loops + 1
			frame = 1
		end
		duration = animation[frame].duration
	end
	return frame, time, loops
end

return MapUtil
